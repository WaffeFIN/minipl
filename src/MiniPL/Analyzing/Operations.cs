﻿using System;
using System.Collections.Generic;
using System.Text;
using MiniPL.Lexing;
using MiniPL.Parsing;

namespace MiniPL.Analyzing
{
    class Operations
    {
        /// <summary>
        /// Converts the given operatorType to a delegate method of object[] -> object type.
        /// </summary>
        /// Ugly, but working solution to the typing issue of the interpreter.
        internal static OperatorNode.Calc GetCalculator(MiniPLTokenType operatorType, ValueType operandType)
        {
            switch (operatorType)
            {
                case MiniPLTokenType.PlusOperator:
                    if (operandType == ValueType.Integer)
                        return (inputs) => {
                            return (long)inputs[0] + (long)inputs[1];
                        };
                    else
                        return (inputs) => {
                            return (string)inputs[0] + (string)inputs[1];
                        };
                case MiniPLTokenType.MinusOperator:
                    return (inputs) => {
                        return (long)inputs[0] - (long)inputs[1];
                    };
                case MiniPLTokenType.TimesOperator:
                    return (inputs) => {
                        return (long)inputs[0] * (long)inputs[1];
                    };
                case MiniPLTokenType.DivisionOperator:
                    return (inputs) => {
                        return (long)inputs[0] / (long)inputs[1];
                    };
                case MiniPLTokenType.LessThanOperator:
                    switch (operandType)
                    {
                        case ValueType.Integer:
                            return (inputs) => {
                                return (long)inputs[0] < (long)inputs[1];
                            };
                        case ValueType.Boolean:
                            return (inputs) => {
                                return !((bool)inputs[0]) && (bool)inputs[1];
                            };
                        default:
                            return (inputs) => {
                                return ((string)inputs[0]).CompareTo((string)inputs[1]) == -1;
                            };
                    }
                case MiniPLTokenType.EqualsOperator:
                    switch (operandType)
                    {
                        case ValueType.Integer:
                            return (inputs) => {
                                return (long)inputs[0] == (long)inputs[1];
                            };
                        case ValueType.Boolean:
                            return (inputs) => {
                                return (bool)inputs[0] == (bool)inputs[1];
                            };
                        default:
                            return (inputs) => {
                                return ((string)inputs[0]).CompareTo((string)inputs[1]) == 0;
                            };
                    }
                case MiniPLTokenType.AndOperator:
                    return (inputs) => {
                        return (bool)inputs[0] && (bool)inputs[1];
                    };
                case MiniPLTokenType.NotOperator:
                    return (inputs) => {
                        return !((bool)inputs[0]);
                    };
                default:
                    return null;
            }
        }
    }
}
