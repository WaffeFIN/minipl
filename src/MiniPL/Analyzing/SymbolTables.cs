using System;
using System.Collections.Generic;
using System.Text;
using MiniPL.Lexing;

namespace MiniPL.Analyzing
{

    public abstract class SymbolTable<K, V> : ISymbolTable<K, V>
    {
        private readonly IDictionary<K, V> variableDeclarations;

        public SymbolTable() {
            variableDeclarations = new Dictionary<K, V>();
        }

        public void Declare(K identifier, V type) {
            variableDeclarations[identifier] = type;
        }


        public bool IsDeclared(K identifier) {
            return variableDeclarations.ContainsKey(identifier);
        }

        public V TypeOf(K identifier) {
            return variableDeclarations[identifier];
        }

        public abstract void EnterScope();
        public abstract void LeaveScope();
    }

    public class DefaultSymbolTable : SymbolTable<string, ValueType>
    {
        public DefaultSymbolTable() : base()
        {
        }

        public override void EnterScope()
        {
            //not used
        }

        public override void LeaveScope()
        {
            //not used
        }
    }

    public class NodeTable<T> : SymbolTable<IASTNode<T>, ValueType>
    {
        public NodeTable() : base()
        {
        }

        public override void EnterScope()
        {
            //not used
        }

        public override void LeaveScope()
        {
            //not used
        }
    }
}