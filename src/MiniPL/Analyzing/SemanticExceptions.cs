using System;
using MiniPL;

namespace MiniPL.Analyzing
{
    /// <summary>
    /// An exception thrown when the variable/expression types mismatch.
    /// </summary>
    public class TypeException : MiniPLException
    {
        public TypeException(string message): base(message) {
        }
    }

    /// <summary>
    /// An exception thrown when an undeclared variable is used.
    /// </summary>
    public class UndeclaredVariableException : MiniPLException
    {
        public UndeclaredVariableException(string message): base(message) {
        }
    }

    /// <summary>
    /// An exception thrown when an variable is declared a second time.
    /// </summary>
    public class DuplicateDeclarationException : MiniPLException
    {
        public DuplicateDeclarationException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// An exception thrown when an variable/expression has an invalid value. Used when no other Exception is suitable.
    /// </summary>
    public class InvalidValueException : MiniPLException
    {
        public InvalidValueException(string message): base(message) {
        }
    }

    /// <summary>
    /// An exception thrown when a constant variable is modified.
    /// </summary>
    public class IllegalModificationException : MiniPLException
    {
        public IllegalModificationException(string message) : base(message)
        {
        }
    }
}