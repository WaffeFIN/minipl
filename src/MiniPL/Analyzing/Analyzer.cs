﻿using System;
using System.Collections.Generic;
using System.Text;
using MiniPL;
using MiniPL.Lexing;
using MiniPL.Parsing;
using MiniPL.Traversing;

namespace MiniPL.Analyzing
{
    class OperatorTypeSignature<T>
    {
        internal readonly T OperatorTokenType;
        internal readonly ValueType[] Inputs;
        internal readonly ValueType Output;

        internal OperatorTypeSignature(T operatorTokenType, ValueType value, ValueType output)
        {
            this.OperatorTokenType = operatorTokenType;
            this.Inputs = new ValueType[] { value };
            this.Output = output;
        }

        internal OperatorTypeSignature(T operatorTokenType, ValueType left, ValueType right, ValueType output)
        {
            this.OperatorTokenType = operatorTokenType;
            this.Inputs = new ValueType[] { left, right };
            this.Output = output;
        }
    }


    public class MiniPLAnalyzer : IMiniPLVisitor, IAnalyzer<MiniPLTokenType>
    {
        private static readonly IList<OperatorTypeSignature<MiniPLTokenType>> operatorSignatures =
            new List<OperatorTypeSignature<MiniPLTokenType>>() {
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.PlusOperator, ValueType.Integer, ValueType.Integer, ValueType.Integer),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.MinusOperator, ValueType.Integer, ValueType.Integer, ValueType.Integer),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.TimesOperator, ValueType.Integer, ValueType.Integer, ValueType.Integer),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.DivisionOperator, ValueType.Integer, ValueType.Integer, ValueType.Integer),

                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.PlusOperator, ValueType.String, ValueType.String, ValueType.String),

                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.AndOperator, ValueType.Boolean, ValueType.Boolean, ValueType.Boolean),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.NotOperator, ValueType.Boolean, ValueType.Boolean),

                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.LessThanOperator, ValueType.String, ValueType.String, ValueType.Boolean),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.LessThanOperator, ValueType.Integer, ValueType.Integer, ValueType.Boolean),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.LessThanOperator, ValueType.Boolean, ValueType.Boolean, ValueType.Boolean),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.EqualsOperator, ValueType.String, ValueType.String, ValueType.Boolean),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.EqualsOperator, ValueType.Integer, ValueType.Integer, ValueType.Boolean),
                new OperatorTypeSignature<MiniPLTokenType>(MiniPLTokenType.EqualsOperator, ValueType.Boolean, ValueType.Boolean, ValueType.Boolean),
            };

        private ISymbolTable<string, ValueType> symbolTable;

        //Since the only constants are single for control variables, a stack can store current constant attributes.
        //As all errors related to constant are dealt with here, the interpreter can ignore these issues.
        private Stack<string> currentConstants;

        private IAnalyzerErrorHandler<MiniPLTokenType> errorHandler;
        public IAnalyzerErrorHandler<MiniPLTokenType> ErrorHandler
        {
            [System.Diagnostics.DebuggerStepThrough]
            set { this.errorHandler = value; }
        }

        public void VisitAll(ISymbolTable<string, ValueType> symbolTable, ITreeTraverser<IASTNode<MiniPLTokenType>> traverser)
        {
            this.symbolTable = symbolTable;
            currentConstants = new Stack<string>();

            IASTNode<MiniPLTokenType> node;
            while ((node = traverser.Next()) != null)
            {
                ((IVisitable<IMiniPLVisitor>)node).Accept(this); //Visitor pattern, uses method polymorphism to replace multiple switch statements
            }
        }

        public void Visit(ProgramNode node)
        {
        }

        public void Visit(StatementsNode node)
        {
        }

        public void Visit(StatementNode node)
        {
        }

        public void Visit(VariableDeclarationStatementNode node)
        {
            var variableIdentifierNode = node.GetIdentifierNode();
            var typeNode = node.GetTypeNode();
            ValueType expected = ValueTypeOfToken(typeNode.Down().Token, node);
            var identifier = variableIdentifierNode.GetIdentifier();
            if (symbolTable.IsDeclared(identifier))
            {
                errorHandler.HandleDuplicateDeclarationException(identifier, node);
            }
            symbolTable.Declare(identifier, expected);
            if (node.HasExpression())
            {
                var valueNode = node.GetExpression();
                ValueType valueType = GetExpressionType(valueNode);
                if (expected != valueType)
                {
                    errorHandler.HandleTypeException(identifier, expected, valueType, node);
                }
            }
        }

        public void Visit(VariableAssignmentStatementNode node)
        {
            var variableIdentifierNode = (VariableIdentifierNode)node.Down();
            ExpressionNode expressionNode = (ExpressionNode)node.Down().Right().Right();
            var identifier = variableIdentifierNode.Down().Token.String;
            if (!symbolTable.IsDeclared(identifier))
            {
                errorHandler.HandleUndeclaredVariableException(identifier, node);
            }
            if (currentConstants.Contains(identifier))
            {
                errorHandler.HandleIllegalModificationException(identifier, node);
            }
            ValueType valueType = GetExpressionType(expressionNode);
            ValueType expected = symbolTable.TypeOf(identifier);
            if (expected != valueType)
            {
                errorHandler.HandleTypeException(identifier, expected, valueType, node);
            }
        }

        public void Visit(ForStatementNode node)
        {
            var variableNode = node.GetIteratingIdentifierNode();
            currentConstants.Push(variableNode.GetIdentifier());
            var variableType = GetExpressionType(variableNode);
            if (variableType != ValueType.Integer)
            {
                errorHandler.HandleTypeException(ValueType.Integer, variableType, variableNode);
            }
            var rangeStart = node.GetRangeStartExpression();
            var rangeStartType = GetExpressionType(rangeStart);
            if (rangeStartType != ValueType.Integer)
            {
                errorHandler.HandleTypeException(ValueType.Integer, rangeStartType, rangeStart);
            }
            var rangeEnd = node.GetRangeEndExpression();
            var rangeEndType = GetExpressionType(rangeEnd);
            if (rangeEndType != ValueType.Integer)
            {
                errorHandler.HandleTypeException(ValueType.Integer, rangeEndType, rangeEnd);
            }
        }

        public void Visit(EndForStatementNode node)
        {
            currentConstants.Pop();
        }

        public void Visit(ReadStatementNode node)
        {
            string identifier = node.GetIdentifier();
            if (currentConstants.Contains(identifier))
            {
                errorHandler.HandleIllegalModificationException(identifier, node);
            }
        }

        public void Visit(PrintStatementNode node)
        {
            GetExpressionType(node.GetExpression());
        }

        public void Visit(AssertStatementNode node)
        {
            ValueType type = GetExpressionType(node.GetExpression());
            if (type != ValueType.Boolean)
            {
                errorHandler.HandleTypeException(ValueType.Boolean, type, node);
            }
        }

        public void Visit(ExpressionNode node)
        {
        }

        public void Visit(OperandNode node)
        {
        }

        public void Visit(OperatorNode node)
        {
            var operatorType = node.Down().Token.Type;
            ValueType operandType = GetExpressionType(node.GetRightOperand());
            OperatorNode.Calc calculator = Operations.GetCalculator(operatorType, operandType);
            if (calculator == null)
                errorHandler.HandleInvalidValueException("Could not get calculator for type '" + operatorType + "' with operandType '" + operandType + '\'', node);
            node.Calculator = calculator;
        }

        public void Visit(TypeNode node)
        {
        }

        public void Visit(VariableIdentifierNode node)
        {
            var tokenNode = (TokenNode)node.Down();
            var identifier = tokenNode.Token.String;
            if (!symbolTable.IsDeclared(identifier))
            {
                errorHandler.HandleUndeclaredVariableException(identifier, node);
            }
        }

        public void Visit(TokenNode node)
        {
        }

        /// <summary>
        /// Recursively retrieves the ValueType of the given ExpressionNode, and stores that info in the given node.
        /// </summary>
        private ValueType GetExpressionType(ExpressionNode node)
        {
            if (node.Type != ValueType.Unknown)
                return node.Type;

            OperatorNode op = node.GetOperator();
            OperandNode left = node.GetFirstOperand();
            if (op == null)
            {
                node.Type = GetExpressionType(left);
                return node.Type;
            }
            else
            {
                OperandNode right = node.GetSecondOperand();
                ValueType[] inputs = right == null ? new ValueType[] { GetExpressionType(left) } : new ValueType[] { GetExpressionType(left), GetExpressionType(right) };
                MiniPLTokenType operatorType = op.Down().Token.Type;
                foreach (OperatorTypeSignature<MiniPLTokenType> operatorSignature in operatorSignatures)
                {
                    if (operatorType == operatorSignature.OperatorTokenType && operatorSignature.Inputs.Length == inputs.Length)
                    {
                        bool match = true;
                        for (int i = 0; i < operatorSignature.Inputs.Length; i++)
                        {
                            if (operatorSignature.Inputs[i] != inputs[i])
                            {
                                match = false;
                                break;
                            }
                        }
                        if (match)
                        {
                            node.Type = operatorSignature.Output;
                            return node.Type;
                        }
                    }
                }
                errorHandler.HandleOperatorTypeException(op, inputs, node);
                return default(ValueType);
            }

        }

        /// <summary>
        /// Recursively retrieves the ValueType of the given OperandNode, and stores that info in the given node.
        /// </summary>
        private ValueType GetExpressionType(OperandNode node)
        {
            if (node.Type == ValueType.Unknown)
            {
                if (node.Down().Right() != null)
                    node.Type = GetExpressionType((ExpressionNode)node.Down().Right());
                else
                {
                    var down = node.Down();
                    if (down.GetType() == typeof(TokenNode))
                        node.Type = GetExpressionType((TokenNode)down);
                    else
                        node.Type = GetExpressionType((VariableIdentifierNode)down);
                }
            }
            return node.Type;
        }

        private ValueType GetExpressionType(VariableIdentifierNode node)
        {
            return GetExpressionType((TokenNode)node.Down());
        }

        private ValueType GetExpressionType(TokenNode node)
        {
            return ValueTypeOfToken(node.Token, node);
        }

        /// <summary>
        /// Converts the given token to an appropriate ValueType.
        /// </summary>
        private ValueType ValueTypeOfToken(IToken<MiniPLTokenType> token, IASTNode<MiniPLTokenType> node)
        {
            if (token.Type == MiniPLTokenType.IntegerValue || token.Type == MiniPLTokenType.IntType)
                return ValueType.Integer;
            if (token.Type == MiniPLTokenType.StringValue || token.Type == MiniPLTokenType.StringType)
                return ValueType.String;
            if (token.Type == MiniPLTokenType.BoolType)
                return ValueType.Boolean;
            if (token.Type == MiniPLTokenType.Identifier)
            {
                string identifier = token.String;
                if (!symbolTable.IsDeclared(identifier))
                    errorHandler.HandleUndeclaredVariableException(identifier, node);
                return symbolTable.TypeOf(identifier);
            }
            errorHandler.HandleInvalidValueException('\'' + token.String + "' can't be converted to a ValueType", node);
            return default(ValueType);
        }
        
    }
}
