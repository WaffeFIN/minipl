﻿using System;
using System.Collections.Generic;
using System.Text;
using MiniPL.Lexing;
using MiniPL.Parsing;
using MiniPL.Analyzing;
using MiniPL.Traversing;

namespace MiniPL
{
    /// <summary>
    /// Handles lexing errors, such as unrecognized tokens.
    /// </summary>
    public interface ILexerErrorHandler
    {
        void HandleUnrecognizedToken(int row, int column, string tokenString);
    }

    /// <summary>
    /// Handles parsing errors, where the recieved token type does not conform to any parse rule.
    /// </summary>
    public interface IParserErrorHandler<T>
    {
        void HandleParseException(int row, int column, MiniPLTokenType[] expected, IToken<T> actual);

        void HandleParseException(int row, int column, MiniPLTokenType expected, IToken<T> actual);

        void HandleParseException(int row, int column, String message, IToken<T> token);
    }

    /// <summary>
    /// Handles semantic errors, i.e. type mismatches, illegal modifiications and variable declaration errors.
    /// </summary>
    public interface IAnalyzerErrorHandler<T>
    {
        void HandleTypeException(string expression, ValueType expected, ValueType type, IASTNode<T> node);

        void HandleTypeException(ValueType expected, ValueType type, IASTNode<T> node);

        void HandleOperatorTypeException(OperatorNode op, ValueType[] inputTypes, IASTNode<T> node); //todo, replace OperatorNode class with generic

        void HandleUndeclaredVariableException(string identifier, IASTNode<T> node);

        void HandleDuplicateDeclarationException(string identifier, IASTNode<T> node);

        void HandleInvalidValueException(string message, IASTNode<T> node);

        void HandleIllegalModificationException(string identifier, IASTNode<T> node);
    }

    /// <summary>
    /// Handles dynamic errors, such as erronous inputs.
    /// </summary>
    public interface IInterpreterErrorHandler
    {
        void HandleInvalidValueException(string expected, string input);
    }

    /// <summary>
    /// Default implementation of MiniPL error handling. 
    /// </summary>
    public partial class MiniPLErrorHandler : ILexerErrorHandler
    {
        public void HandleUnrecognizedToken(int row, int column, string tokenString)
        {
            throw new TokenRecognitionException("ERROR @" + row + ":" + column +
                " -- Could not tokenize the string '" + tokenString + "' as a valid token");
        }
    }

    public partial class MiniPLErrorHandler : IParserErrorHandler<MiniPLTokenType>, IAnalyzerErrorHandler<MiniPLTokenType>, IInterpreterErrorHandler
    {
        public void HandleParseException(int row, int column, MiniPLTokenType[] expected, IToken<MiniPLTokenType> actual)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < expected.Length - 1; i++)
            {
                builder.Append('\'');
                builder.Append(expected[i]);
                builder.Append("', ");
            }
            builder.Append('\'');
            builder.Append(expected[expected.Length - 1]);
            builder.Append("'");
            HandleParseException(row, column, "Expected " + builder.ToString() + " but got ", actual);
        }

        public void HandleParseException(int row, int column, MiniPLTokenType expected, IToken<MiniPLTokenType> actual)
        {
            HandleParseException(row, column, "Expected '" + expected + "' but got ", actual);
        }

        public void HandleParseException(int row, int column, String message, IToken<MiniPLTokenType> token)
        {
            throw new ParseException("ERROR @" + row + ":" + column +
                " -- " + message + '\'' + token.Type +
                "' with string '" + token.String + '\'');
        }
    }

    public partial class MiniPLErrorHandler : IAnalyzerErrorHandler<MiniPLTokenType>
    {
        public void HandleTypeException(string expression, ValueType expected, ValueType type, IASTNode<MiniPLTokenType> node)
        {
            throw new TypeException("ERROR @" + node.Row + ':' + node.Column + "-- Variable or expression '" + expression + "' has type '" +
                type + "' but is used as a '" + expected + "'.\n" +
                "\tLook @ \"" + StringifyNode(node) + "\"");
        }

        public void HandleTypeException(ValueType expected, ValueType type, IASTNode<MiniPLTokenType> node)
        {
            throw new TypeException("ERROR @" + node.Row + ':' + node.Column + "-- Expression or variable has type '" + type + "' but should be '" + expected + "'.\n" +
                "\tLook @ \"" + StringifyNode(node) + "\"");
        }

        public void HandleOperatorTypeException(OperatorNode op, ValueType[] inputTypes, IASTNode<MiniPLTokenType> node)
        {
            throw new TypeException("ERROR @" + node.Row + ':' + node.Column + "-- Operator '" + StringifyNode(op) + "' does not match inputs of types '" + inputTypes.ToString() + "'.\n" +
                "\tLook @ \"" + StringifyNode(node) + "\"");
        }

        public void HandleUndeclaredVariableException(string identifier, IASTNode<MiniPLTokenType> node)
        {
            throw new UndeclaredVariableException("ERROR @" + node.Row + ':' + node.Column + "-- Variable '" + identifier + "' has not been declared.\n" +
                "\tLook @ \"" + StringifyNode(node) + "\"");
        }

        public void HandleDuplicateDeclarationException(string identifier, IASTNode<MiniPLTokenType> node)
        {
            throw new DuplicateDeclarationException("ERROR @" + node.Row + ':' + node.Column + "-- Variable '" + identifier + "' is declared twice.\n" +
                "\tLook @ \"" + StringifyNode(node) + "\"");
        }

        public void HandleInvalidValueException(string message, IASTNode<MiniPLTokenType> node)
        {
            throw new InvalidValueException("ERROR @" + node.Row + ':' + node.Column + "-- " + message + ".\n" +
                "\tLook @ \"" + StringifyNode(node) + "\"");
        }

        //when modifying the for control variable
        public void HandleIllegalModificationException(string identifier, IASTNode<MiniPLTokenType> node)
        {
            throw new IllegalModificationException("ERROR @" + node.Row + ':' + node.Column + "-- The variable '" + identifier + "' is modified while it's iterated upon.\n" +
                "\tLook @ \"" + StringifyNode(node) + "\"");
        }

        private string StringifyNode(IASTNode<MiniPLTokenType> node)
        {
            var traverser = new DepthFirstPostOrderTreeTraverser<IASTNode<MiniPLTokenType>>(node);
            StringBuilder builder = new StringBuilder();
            bool first = true;
            while ((node = traverser.Next()) != null)
            {
                if (node.Token != null)
                {
                    if (!first)
                    {
                        builder.Append(' ');
                    }
                    else
                    {
                        first = false;
                    }
                    builder.Append(node.Token.String);
                }
            }
            return builder.ToString();
        }
    }

    public partial class MiniPLErrorHandler : IInterpreterErrorHandler
    {
        public void HandleInvalidValueException(string expected, string input)
        {
            throw new InvalidValueException("ERROR -- Input is not in " + expected + " format '" + input + '\'');
        }
    }
}