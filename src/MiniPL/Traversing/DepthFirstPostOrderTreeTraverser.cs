using System;
using System.Collections.Generic;
using System.Text;

namespace MiniPL.Traversing
{
    /// <summary>
    /// Traverses the given tree in depth-first post-order.
    /// </summary>
    public class DepthFirstPostOrderTreeTraverser<T> : ITreeTraverser<T> where T : ITreeNode<T> {
        
        private T currentNode;

        public DepthFirstPostOrderTreeTraverser(T root) {
            currentNode = Lowest(root);
        }

        private T Lowest(T node) {
            while(node.Down() != null)
                node = node.Down();
            return node;
        }

        public T Next() {
            T returnValue = currentNode;
            if (currentNode != null) {
                if (currentNode.Right() != null) {
                    currentNode = Lowest(currentNode.Right());
                } else {
                    currentNode = currentNode.Up();
                }
            }
            return returnValue;
        }
    }
}