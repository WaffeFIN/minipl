using System;
using System.Collections.Generic;
using System.Text;

namespace MiniPL.Traversing
{
    /// <summary>
    /// Traverses the given tree in depth-first pre-order.
    /// </summary>
    public class DepthFirstPreOrderTreeTraverser<T> : ITreeTraverser<T> where T : ITreeNode<T> {
        
        private T currentNode;
        private readonly T root;

        public DepthFirstPreOrderTreeTraverser(T root) {
            this.root = root;
            currentNode = root;
        }

        //check IEnumerator

        private T ClosestSiblingOfAncestor(T node) {
            while(node.Right() == null && node.Up() != null && !root.Equals(node.Up()))
                node = node.Up();
            return node.Right();
        }

        public T Next() {
            T returnValue = currentNode;
            if (currentNode != null) {
                if (currentNode.Down() != null) {
                    currentNode = currentNode.Down();
                } else {
                    currentNode = ClosestSiblingOfAncestor(currentNode);
                }
            }
            return returnValue;
        }
    }
}