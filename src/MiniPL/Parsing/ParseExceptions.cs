using System;
using MiniPL;

namespace MiniPL.Parsing
{
    public class ParseException : MiniPLException
    {
        /// <summary>
        /// Exception thrown when an IParser recieves an unexpected token.
        /// </summary>
        public ParseException(string message) : base(message)
        {
        }
    }
}