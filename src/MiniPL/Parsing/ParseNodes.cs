﻿using System;
using System.Collections.Generic;
using System.Text;
using MiniPL.Lexing;

namespace MiniPL.Parsing
{
    public class ProgramNode : MiniPLASTNode
    {
        internal ProgramNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class StatementsNode : MiniPLASTNode
    {
        internal StatementsNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class StatementNode : MiniPLASTNode
    {
        internal StatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class VariableDeclarationStatementNode : MiniPLASTNode
    {
        internal VariableDeclarationStatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public string GetIdentifier()
        {
            return GetIdentifierNode().GetIdentifier();
        }

        public VariableIdentifierNode GetIdentifierNode()
        {
            return (VariableIdentifierNode)Down().Right();
        }

        public TypeNode GetTypeNode()
        {
            return (TypeNode)Down().Right().Right().Right();
        }

        public bool HasExpression()
        {
            return Down().Right().Right().Right().Right() != null;
        }

        public ExpressionNode GetExpression()
        {
            if (!HasExpression())
                return null;
            return (ExpressionNode)Down().Right().Right().Right().Right().Right();
        }
    }

    public class VariableAssignmentStatementNode : MiniPLASTNode
    {
        internal VariableAssignmentStatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public string GetIdentifier()
        {
            return ((VariableIdentifierNode)Down()).GetIdentifier();
        }

        public ExpressionNode GetExpression()
        {
            return (ExpressionNode)Down().Right().Right();
        }
    }

    public class ForStatementNode : MiniPLASTNode
    {
        internal ForStatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public string GetIteratingIdentifier()
        {
            return GetIteratingIdentifierNode().GetIdentifier();
        }

        public VariableIdentifierNode GetIteratingIdentifierNode()
        {
            return (VariableIdentifierNode)Down().Right();
        }

        public ExpressionNode GetRangeStartExpression()
        {
            return (ExpressionNode)Down().Right().Right().Right();
        }

        public ExpressionNode GetRangeEndExpression()
        {
            return (ExpressionNode)Down().Right().Right().Right().Right().Right();
        }

        public StatementsNode GetStatements()
        {
            return (StatementsNode)Down().Right().Right().Right().Right().Right().Right().Right();
        }
    }

    public class EndForStatementNode : MiniPLASTNode
    {
        internal EndForStatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class ReadStatementNode : MiniPLASTNode
    {
        internal ReadStatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public string GetIdentifier()
        {
            return ((VariableIdentifierNode)Down().Right()).GetIdentifier();
        }
    }

    public class PrintStatementNode : MiniPLASTNode
    {
        internal PrintStatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public ExpressionNode GetExpression()
        {
            return (ExpressionNode)Down().Right();
        }
    }

    public class AssertStatementNode : MiniPLASTNode
    {
        internal AssertStatementNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public ExpressionNode GetExpression()
        {
            return (ExpressionNode)Down().Right().Right();
        }
    }

    public class ExpressionNode : MiniPLASTNode
    {
        private ValueType type;

        internal ExpressionNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public ValueType Type
        {
            get { return type; }
            set { type = value; }
        }

        private T GetNodeWithType<T>(int which)
        {
            int n = 0;
            var node = this.Down();
            while (node != null)
            {
                if (node.GetType() == typeof(T))
                {
                    if (n == which)
                        return (T)node;
                    n++;
                }
                node = node.Right();
            }
            return default(T);
        }

        public OperandNode GetFirstOperand()
        {
            return GetNodeWithType<OperandNode>(0);
        }

        public OperandNode GetSecondOperand()
        {
            return GetNodeWithType<OperandNode>(1);
        }

        public OperatorNode GetOperator()
        {
            return GetNodeWithType<OperatorNode>(0);
        }

        public bool HasOperator()
        {
            return GetOperator() != null;
        }

        public bool HasSecondOperand()
        {
            return GetSecondOperand() != null;
        }
    }

    public class OperandNode : MiniPLASTNode
    {
        private ValueType type;

        internal OperandNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public ValueType Type
        {
            get { return type; }
            set { type = value; }
        }

        public bool HasExpression()
        {
            return Down().Right() != null;
        }

        public ExpressionNode GetExpression()
        {
            return (ExpressionNode)Down().Right();
        }

        public bool HasIdentifierNode()
        {
            return Down().GetType() == typeof(VariableIdentifierNode);
        }

        public VariableIdentifierNode GetIdentifierNode()
        {
            return (VariableIdentifierNode) Down();
        }
    }

    public class OperatorNode : MiniPLASTNode
    {
        public delegate object Calc(object[] inputs);

        private Calc calculator;

        internal OperatorNode(int column, int row) : base(column, row) { }

        public Calc Calculator
        {
            set { calculator = value; }
        }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public object Calculate(object[] inputs)
        {
            return calculator(inputs);
        }

        public OperandNode GetRightOperand()
        {
            return (OperandNode) Right();
        }
    }

    public class TypeNode : MiniPLASTNode
    {
        internal TypeNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    public class VariableIdentifierNode : MiniPLASTNode
    {
        internal VariableIdentifierNode(int column, int row) : base(column, row) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }

        public string GetIdentifier()
        {
            return Down().Token.String;
        }
    }

    public class TokenNode : MiniPLASTNode
    {
        internal TokenNode(int column, int row, IToken<MiniPLTokenType> token) : base(column, row, token) { }

        public override void Accept(IMiniPLVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
