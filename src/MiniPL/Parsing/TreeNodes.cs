﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniPL.Parsing
{
    /// <summary>
    /// Default implementation of ITreeNode<typeparamref name="T"/>
    /// </summary>
    public class TreeNode<T> : ITreeNode<T> where T : ITreeNode<T>
    {
        private T down;
        private T right;
        private T up;

        public T DownRight()
        {
            T rightmostChild = down;
            while (rightmostChild.Right() != null)
            {
                rightmostChild = rightmostChild.Right();
            }
            return rightmostChild;
        }

        public void Down(T down)
        {
            this.down = down;
        }

        public T Down()
        {
            return down;
        }

        public void Right(T right)
        {
            this.right = right;
        }

        public T Right()
        {
            return right;
        }

        public void Up(T up)
        {
            this.up = up;
        }

        public T Up()
        {
            return up;
        }
    }

    /// <summary>
    /// Default implementation of IASTNode<typeparamref name="T"/>, extending upon TreeNode
    /// </summary>
    public class ASTNode<T> : TreeNode<IASTNode<T>>, IASTNode<T>
    {
        private IToken<T> token;
        private readonly int column;
        private readonly int row;

        public ASTNode(int column, int row) { this.column = column; this.row = row; }
        public ASTNode(int column, int row, IToken<T> token) { this.column = column; this.row = row; this.token = token; }

        public IToken<T> Token
        {
            [System.Diagnostics.DebuggerStepThrough]
            get { return token; }
            set { this.token = value; }
        }

        public int Column => column;

        public int Row => row;
    }

    /// <summary>
    /// The ASTNode used by the MiniPLParser, -Analyzer and -Interpreter. Extends ASTNode<MiniPL.Lexing.MiniPLTokenType> and is visitable by IMiniPLVisitor.
    /// </summary>
    public abstract class MiniPLASTNode : ASTNode<MiniPL.Lexing.MiniPLTokenType>, IVisitable<IMiniPLVisitor>
    {
        public MiniPLASTNode(int column, int row) : base(column, row) { }
        public MiniPLASTNode(int column, int row, IToken<MiniPL.Lexing.MiniPLTokenType> token) : base(column, row, token) { }

        public abstract void Accept(IMiniPLVisitor visitor);
    }
}
