using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using MiniPL.Lexing;

namespace MiniPL.Parsing
{
    //  public class Tester
    //  {
    //      public static void Main(string[] args) {
    //          string code = "var X : string ; for X in (1)..4 + X do print \"hi\"; print X; assert ( \"hi\" = \"hi\" ); end for;";
    //          new MiniPLParser(new MiniPLLexer()).Parse(new StringReader(code));
    //      }
    //  }

    public class MiniPLParser : IParser<MiniPLTokenType>
    {
        private readonly ILexer<MiniPLTokenType> lexer;
        private IASTNode<MiniPLTokenType> currentNode;
        private IToken<MiniPLTokenType> lookahead;

        public MiniPLParser(ILexer<MiniPLTokenType> lexer)
        {
            this.lexer = lexer;
        }

        private IParserErrorHandler<MiniPLTokenType> errorHandler;
        public IParserErrorHandler<MiniPLTokenType> ErrorHandler
        {
            [System.Diagnostics.DebuggerStepThrough]
            set { this.errorHandler = value; }
        }

        private IToken<MiniPLTokenType> Peek()
        {
            return lookahead;
        }

        private IToken<MiniPLTokenType> Advance()
        {
            IToken<MiniPLTokenType> next = lookahead;
            lookahead = lexer.Next();
            return next;
        }

        private bool HasNext()
        {
            return Peek().Type != MiniPLTokenType.EOF;
        }

        public IASTNode<MiniPLTokenType> Parse(TextReader reader)
        {
            lexer.Init(reader);
            currentNode = new ProgramNode(lexer.Column, lexer.Row);
            var root = currentNode;
            Advance();
            Statements();
            if (Peek().Type != MiniPLTokenType.EOF)
            {
                errorHandler.HandleParseException(lexer.Column, lexer.Row, MiniPLTokenType.EOF, Peek());
            }
            System.Diagnostics.Debug.Assert(root == currentNode, "The root of the AST must be the currentNode at the end of parsing!");
            return root;
        }

        private void Statements()
        {
            NextNode(new StatementsNode(lexer.Column, lexer.Row));
            do
            {
                Statement();
                Expect(MiniPLTokenType.Semicolon);
            } while (HasNextStatement());
            PreviousNode();
        }

        private void Statement()
        {
            NextNode(new StatementNode(lexer.Column, lexer.Row));
            switch (Peek().Type)
            {
                case MiniPLTokenType.Var:
                    VariableDeclaration();
                    break;
                case MiniPLTokenType.For:
                    ForStatement();
                    break;
                case MiniPLTokenType.Read:
                    Read();
                    break;
                case MiniPLTokenType.Print:
                    Print();
                    break;
                case MiniPLTokenType.Assert:
                    Assert();
                    break;
                case MiniPLTokenType.Identifier:
                    Assignment();
                    break;
                default:
                    errorHandler.HandleParseException(lexer.Column, lexer.Row,
                        new MiniPLTokenType[]{
                            MiniPLTokenType.Var,
                            MiniPLTokenType.For,
                            MiniPLTokenType.Read,
                            MiniPLTokenType.Print,
                            MiniPLTokenType.Assert,
                            MiniPLTokenType.Identifier
                        }, Peek());
                    break;
            }
            PreviousNode();
        }

        private bool HasNextStatement()
        { //ew...
            switch (Peek().Type)
            {
                case MiniPLTokenType.Var:
                case MiniPLTokenType.For:
                case MiniPLTokenType.Read:
                case MiniPLTokenType.Print:
                case MiniPLTokenType.Assert:
                case MiniPLTokenType.Identifier:
                    return true;
                default:
                    return false;
            }
        }

        private void VariableDeclaration()
        {
            NextNode(new VariableDeclarationStatementNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.Var);
            VarIdent();
            Expect(MiniPLTokenType.Colon);
            Type();
            if (Peek().Type == MiniPLTokenType.AssignmentOperator)
            {
                Expect(MiniPLTokenType.AssignmentOperator);
                Expression();
            }
            PreviousNode();
        }

        private void ForStatement()
        {
            NextNode(new ForStatementNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.For);
            VarIdent();
            Expect(MiniPLTokenType.In);
            Expression();
            Expect(MiniPLTokenType.DotDot);
            Expression();
            Expect(MiniPLTokenType.Do);
            Statements();
            EndFor();
            PreviousNode();
        }

        private void EndFor()
        {
            NextNode(new EndForStatementNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.End);
            Expect(MiniPLTokenType.For);
            PreviousNode();
        }

        private void Read()
        {
            NextNode(new ReadStatementNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.Read);
            VarIdent();
            PreviousNode();
        }

        private void Print()
        {
            NextNode(new PrintStatementNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.Print);
            Expression();
            PreviousNode();
        }

        private void Assert()
        {
            NextNode(new AssertStatementNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.Assert);
            Expect(MiniPLTokenType.ParenthesisOpen);
            Expression();
            Expect(MiniPLTokenType.ParenthesisClose);
            PreviousNode();
        }

        private void Assignment()
        {
            NextNode(new VariableAssignmentStatementNode(lexer.Column, lexer.Row));
            VarIdent();
            Expect(MiniPLTokenType.AssignmentOperator);
            Expression();
            PreviousNode();
        }

        private void Expression()
        {
            NextNode(new ExpressionNode(lexer.Column, lexer.Row));
            if (HasNextUnaryOperator())
            {
                UnaryOperator();
                Operand();
            }
            else
            {
                Operand();
                if (HasNextOperator())
                {
                    Operator();
                    Operand();
                }
            }
            PreviousNode();
        }

        private void Operand()
        {
            NextNode(new OperandNode(lexer.Column, lexer.Row));
            if (Peek().Type == MiniPLTokenType.ParenthesisOpen)
            {
                Expect(MiniPLTokenType.ParenthesisOpen);
                Expression();
                Expect(MiniPLTokenType.ParenthesisClose);
            }
            else
            {
                if (Peek().Type == MiniPLTokenType.Identifier)
                    VarIdent();
                else
                    Expect(new MiniPLTokenType[]{
                        MiniPLTokenType.IntegerValue,
                        MiniPLTokenType.StringValue
                    });
            }
            PreviousNode();
        }

        private bool HasNextUnaryOperator()
        {
            return Peek().Type == MiniPLTokenType.NotOperator;
        }

        private void UnaryOperator()
        {
            NextNode(new OperatorNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.NotOperator);
            PreviousNode();
        }

        private bool HasNextOperator()
        {
            switch (Peek().Type)
            {
                case MiniPLTokenType.PlusOperator:
                case MiniPLTokenType.MinusOperator:
                case MiniPLTokenType.TimesOperator:
                case MiniPLTokenType.DivisionOperator:
                case MiniPLTokenType.LessThanOperator:
                case MiniPLTokenType.EqualsOperator:
                case MiniPLTokenType.AndOperator:
                    return true;
            }
            return false;
        }

        private void Operator()
        {
            NextNode(new OperatorNode(lexer.Column, lexer.Row));
            Expect(new MiniPLTokenType[]{
                MiniPLTokenType.PlusOperator,
                MiniPLTokenType.MinusOperator,
                MiniPLTokenType.TimesOperator,
                MiniPLTokenType.DivisionOperator,
                MiniPLTokenType.LessThanOperator,
                MiniPLTokenType.EqualsOperator,
                MiniPLTokenType.AndOperator
            });
            PreviousNode();
        }

        private void Type()
        {
            NextNode(new TypeNode(lexer.Column, lexer.Row));
            Expect(new MiniPLTokenType[]{
                MiniPLTokenType.IntType,
                MiniPLTokenType.StringType,
                MiniPLTokenType.BoolType
            });
            PreviousNode();
        }

        private void VarIdent()
        {
            NextNode(new VariableIdentifierNode(lexer.Column, lexer.Row));
            Expect(MiniPLTokenType.Identifier);
            PreviousNode();
        }

        private void Expect(MiniPLTokenType type)
        {
            if (Peek().Type != type)
                errorHandler.HandleParseException(lexer.Column, lexer.Row, type, Peek());
            NextNode(new TokenNode(lexer.Column, lexer.Row, Peek()));
            PreviousNode();
            Advance();
        }

        private void Expect(MiniPLTokenType[] types)
        {
            bool contained = false;
            for (int i = 0; i < types.Length; i++)
            {
                if (Peek().Type == types[i])
                {
                    contained = true;
                    break;
                }
            }
            if (!contained)
            {
                errorHandler.HandleParseException(lexer.Column, lexer.Row, types, Peek());
            }
            NextNode(new TokenNode(lexer.Column, lexer.Row, Peek()));
            PreviousNode();
            Advance();
        }

        private void NextNode(IASTNode<MiniPLTokenType> node)
        {
            if (currentNode.Down() != null)
            {
                currentNode.DownRight().Right(node);
            }
            else
            {
                currentNode.Down(node);
            }
            node.Up(currentNode);
            currentNode = node;
        }

        private void PreviousNode()
        {
            currentNode = currentNode.Up();
        }
    }
}