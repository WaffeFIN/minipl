﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace MiniPL.Lexing
{
    public class MiniPLLexer : ILexer<MiniPLTokenType>
    {
        private TextReader reader;
        private int row;
        private int column;
        private ILexerErrorHandler errorHandler;

        private readonly IList<Pair<IStringMatcher, MiniPLTokenType>> matchers;
        private readonly ISet<MiniPLTokenType> skip;

        private class Pair<K, V> {
            internal readonly K Key;
            internal readonly V Value;

            internal Pair(K key, V value) {
                this.Key = key;
                this.Value = value;
            }
        }

        private static readonly int EOF = -1;

        public MiniPLLexer() {
            IntRange[] DIGITS = new IntRange[] {new IntRange('0', '9')};
            IntRange[] STRING_CHARACTERS = new IntRange[] {
                new IntRange(' ', '"' - 1), new IntRange('"' + 1, '\\' - 1), new IntRange('\\' + 1, '~'), //no backslash nor quote
                new IntRange('\n', '\n'), new IntRange('\t', '\t'), new IntRange('\r', '\r')}; //whitespace chars
            IntRange[] LETTERS = new IntRange[] { new IntRange('a', 'z'), new IntRange('A', 'Z') };
            IntRange[] IDENTIFIER_CHARACTERS = new IntRange[] { new IntRange('0', '9'), new IntRange('a', 'z'), new IntRange('A', 'Z'), new IntRange('_', '_') };
            HashSet<int> WHITESPACE = new HashSet<int>(){' ', '\n', '\r', '\t'};

            matchers = new List<Pair<IStringMatcher, MiniPLTokenType>>
            {
                new Pair<IStringMatcher, MiniPLTokenType>(
                    new GroupMatcher(new IStringMatcher[]{
                        new EqualityMatcher("\""),
                        new RestrictedAlphabetWithSubstringsMatcher(
                            new RestrictedAlphabetMatcher(STRING_CHARACTERS),
                            new IStringMatcher[]{
                                new EqualityMatcher("\\\\"),
                                new EqualityMatcher("\\\""),
                                new EqualityMatcher("\\n"),
                                new EqualityMatcher("\\t"),
                                new EqualityMatcher("\\b"),
                                new EqualityMatcher("\\r"),
                                new EqualityMatcher("\\f"),
                        }),
                        new EqualityMatcher("\"")
                    }), MiniPLTokenType.StringValue),
                new Pair<IStringMatcher, MiniPLTokenType>(new DeepGroupMatcher(new IStringMatcher[]{
                    new EqualityMatcher("/*"),
                    new NotContainsMatcher("*"),
                    new EqualityMatcher("*/")
                }), MiniPLTokenType.MultilineComment),
                new Pair<IStringMatcher, MiniPLTokenType>(new GroupMatcher(new IStringMatcher[]{
                    new EqualityMatcher("//"),
                    new NotContainsMatcher("\n"),
                    new EqualityMatcher("\n")
                }), MiniPLTokenType.CommentedLine),
                new Pair<IStringMatcher, MiniPLTokenType>(new RestrictedAlphabetMatcher(DIGITS), MiniPLTokenType.IntegerValue),
                new Pair<IStringMatcher, MiniPLTokenType>(new RestrictedAlphabetMatcher(WHITESPACE), MiniPLTokenType.Whitespace),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("assert"), MiniPLTokenType.Assert),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("string"), MiniPLTokenType.StringType),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("print"), MiniPLTokenType.Print),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("bool"), MiniPLTokenType.BoolType),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("read"), MiniPLTokenType.Read),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("end"), MiniPLTokenType.End),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("for"), MiniPLTokenType.For),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("int"), MiniPLTokenType.IntType),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("var"), MiniPLTokenType.Var),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("in"), MiniPLTokenType.In),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("do"), MiniPLTokenType.Do),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher(":="), MiniPLTokenType.AssignmentOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher(".."), MiniPLTokenType.DotDot),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher(":"), MiniPLTokenType.Colon),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("+"), MiniPLTokenType.PlusOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("-"), MiniPLTokenType.MinusOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("*"), MiniPLTokenType.TimesOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("/"), MiniPLTokenType.DivisionOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("<"), MiniPLTokenType.LessThanOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("="), MiniPLTokenType.EqualsOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("&"), MiniPLTokenType.AndOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("!"), MiniPLTokenType.NotOperator),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher(";"), MiniPLTokenType.Semicolon),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher("("), MiniPLTokenType.ParenthesisOpen),
                new Pair<IStringMatcher, MiniPLTokenType>(new EqualityMatcher(")"), MiniPLTokenType.ParenthesisClose),
                new Pair<IStringMatcher, MiniPLTokenType>(new GroupMatcher(new IStringMatcher[]{
                    new NonEmptyRestrictedAlphabetMatcher(LETTERS),
                    new RestrictedAlphabetMatcher(IDENTIFIER_CHARACTERS)
                }), MiniPLTokenType.Identifier)
            };

            skip = new HashSet<MiniPLTokenType>(){
                MiniPLTokenType.Whitespace,
                MiniPLTokenType.CommentedLine,
                MiniPLTokenType.MultilineComment
            };
        }

        public void Init(TextReader reader)
        {
            this.reader = reader;
            this.row = 1;
            this.column = 1;
        }

        public int Row
        {
            [System.Diagnostics.DebuggerStepThrough]
            get { return row; }
        }

        public int Column
        {
            [System.Diagnostics.DebuggerStepThrough]
            get { return column; }
        }

        public ILexerErrorHandler ErrorHandler
        {
            [System.Diagnostics.DebuggerStepThrough]
            set { this.errorHandler = value; }
        }

        public bool HasNext()
        {
            return reader.Peek() != -1;
        }

        public IToken<MiniPLTokenType> Next()
        {
            IToken<MiniPLTokenType> token;
            do {
                token = ReadToken();
            } while(skip.Contains(token.Type));
            return token;
        }

        private int PeekChar() {
            return reader.Peek();
        }

        private int ReadChar() {
            int c = reader.Read();
            if (c == '\n') {    
                column = 1;
                row++;
            } else {
                column++;
            }
            return c;
        }

        private IToken<MiniPLTokenType> ReadToken() {
            if (!HasNext())
                return new MiniPLToken(MiniPLTokenType.EOF, "");

            ResetMatchers();
            var matching = matchers;
            var result = new List<Pair<IStringMatcher, MiniPLTokenType>>();
            var builder = new StringBuilder();
            int c = PeekChar();
            while (c != EOF)
            {
                var nextMatching = new List<Pair<IStringMatcher, MiniPLTokenType>>();
                bool clearResults = true;
                foreach (Pair<IStringMatcher, MiniPLTokenType> pair in matching) {
                    var matcher = pair.Key;
                    MatchType match = matcher.Match((char) c);
                    if (match == MatchType.MATCH) {
                        nextMatching.Add(pair);
                        if (clearResults) {
                            result.Clear();
                            clearResults = false;
                        }
                        result.Add(pair);
                    } else if (match == MatchType.PARTIAL_MATCH) {
                        nextMatching.Add(pair);
                    }
                }
                if (nextMatching.Count == 0)
                    break;
                builder.Append((char) c);
                ReadChar(); c = PeekChar(); //one character lookahead
                matching = nextMatching;
            }
            if (result.Count == 0)
                errorHandler.HandleUnrecognizedToken(row, column, builder.ToString());
            MiniPLTokenType tokenType = result[0].Value; //choose the most prioritized matcher
            return new MiniPLToken(tokenType, builder.ToString());
        }

        private void ResetMatchers()
        {
            foreach (Pair<IStringMatcher, MiniPLTokenType> pair in matchers)
            {
                pair.Key.Reset();
            }
        }
    }
}
