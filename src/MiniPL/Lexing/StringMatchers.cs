using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MiniPL.Lexing
{
    public enum MatchType
    {
        MATCH,
        PARTIAL_MATCH,
        NO_MATCH
    }

    public interface IStringMatcher
    {
        void Reset();
        MatchType Match(char c);
        MatchType MatchEmpty();
        IStringMatcher Clone();
    }

    public class EqualityMatcher : IStringMatcher
    {
        private readonly string matchee;
        private int charIndex;

        public EqualityMatcher(string matchee)
        {
            this.matchee = matchee;
            this.charIndex = -1;
        }

        public void Reset()
        {
            this.charIndex = -1;
        }

        public MatchType Match(char c)
        {
            charIndex++;
            if (charIndex < matchee.Length && c == matchee[charIndex])
                if (charIndex == matchee.Length - 1)
                    return MatchType.MATCH;
                else
                    return MatchType.PARTIAL_MATCH;
            else
            {
                charIndex = matchee.Length;
                return MatchType.NO_MATCH;
            }
        }

        public MatchType MatchEmpty()
        {
            return matchee == "" ? MatchType.MATCH : MatchType.NO_MATCH;
        }

        public IStringMatcher Clone()
        {
            return new EqualityMatcher(matchee);
        }
    }

    public abstract class AlphabetMatcher : IStringMatcher
    {

        protected readonly ISet<int> alphabet;
        private MatchType state;

        public AlphabetMatcher(ISet<int> alphabet)
        {
            this.alphabet = alphabet;
            this.state = MatchType.MATCH;
        }

        internal AlphabetMatcher(IntRange[] ranges)
        {
            this.alphabet = ranges[0].AsSet();
            for (int i = 1; i < ranges.Length; i++)
            {
                this.alphabet.UnionWith(ranges[i].AsSet());
            }
            this.state = MatchType.MATCH;
        }

        public void Reset()
        {
            this.state = MatchType.MATCH;
        }

        public MatchType Match(char c)
        {
            if (!alphabet.Contains(c))
                state = MatchType.NO_MATCH;
            return state;
        }

        abstract public MatchType MatchEmpty();

        abstract public IStringMatcher Clone();
    }


    public class RestrictedAlphabetMatcher : AlphabetMatcher
    {
        public RestrictedAlphabetMatcher(ISet<int> alphabet) : base(alphabet)
        {
        }

        internal RestrictedAlphabetMatcher(IntRange[] ranges) : base(ranges)
        {
        }

        public override MatchType MatchEmpty()
        {
            return MatchType.MATCH;
        }

        public override IStringMatcher Clone()
        {
            return new RestrictedAlphabetMatcher(alphabet);
        }
    }

    public class NonEmptyRestrictedAlphabetMatcher : AlphabetMatcher
    {
        public NonEmptyRestrictedAlphabetMatcher(ISet<int> alphabet) : base(alphabet)
        {
        }

        internal NonEmptyRestrictedAlphabetMatcher(IntRange[] ranges) : base(ranges)
        {
        }

        public override MatchType MatchEmpty()
        {
            return MatchType.NO_MATCH;
        }

        public override IStringMatcher Clone()
        {
            return new NonEmptyRestrictedAlphabetMatcher(alphabet);
        }
    }

    public class RestrictedAlphabetWithSubstringsMatcher : IStringMatcher
    {
        private readonly AlphabetMatcher baseMatcher;
        private readonly IStringMatcher[] extraMatchers;
        private MatchType emptyMatchWithExtras;
        private bool baseFailed;

        public RestrictedAlphabetWithSubstringsMatcher(AlphabetMatcher baseMatcher,  IStringMatcher[] extraMatchers)
        {
            this.baseMatcher = baseMatcher;
            this.extraMatchers = extraMatchers;
            this.baseFailed = false;
            this.emptyMatchWithExtras = MatchAnyEmpty();
        }

        private MatchType MatchAnyEmpty()
        {
            foreach (IStringMatcher matcher in extraMatchers)
            {
                if (matcher.MatchEmpty() == MatchType.MATCH)
                {
                    return MatchType.MATCH;
                }
            }
            return MatchType.NO_MATCH;
        }

        public MatchType Match(char c)
        {
            if (!baseFailed && baseMatcher.Match(c) == MatchType.MATCH)
                return MatchType.MATCH;
            else
            {
                baseFailed = true;
                MatchType match = MatchType.NO_MATCH;
                foreach (IStringMatcher matcher in extraMatchers)
                {
                    MatchType currentMatch = matcher.Match(c);
                    switch (currentMatch)
                    {
                        case MatchType.MATCH:
                            match = MatchType.MATCH;
                            break;
                        case MatchType.PARTIAL_MATCH:
                            if (match == MatchType.NO_MATCH)
                                match = MatchType.PARTIAL_MATCH;
                            break;
                        case MatchType.NO_MATCH:
                            break;
                    }
                }
                if (match == MatchType.MATCH)
                    Reset();
                return match;
            }
        }

        public MatchType MatchEmpty()
        {
            return baseMatcher.MatchEmpty();
        }

        public void Reset()
        {
            foreach (IStringMatcher matcher in extraMatchers)
            {
                matcher.Reset();
            }
            baseMatcher.Reset();
            baseFailed = false;
        }

        public IStringMatcher Clone()
        {
            IStringMatcher[] newMatchers = new IStringMatcher[extraMatchers.Length];
            for (int i = 0; i < extraMatchers.Length; i++)
            {
                newMatchers[i] = extraMatchers[i].Clone();
            }
            return new GroupMatcher(newMatchers);
        }
    }

    public class GroupMatcher : IStringMatcher
    {

        protected readonly IStringMatcher[] matchers;
        protected int state;
        protected bool hasMatched;
        private MatchType emptyMatch;

        public GroupMatcher(IStringMatcher[] matchers)
        {
            this.matchers = matchers;
            this.state = 0;
            this.hasMatched = false;
            this.emptyMatch = MatchRestEmpty(0);
        }

        protected MatchType MatchRestEmpty(int state)
        {
            var match = MatchType.MATCH;
            for (int i = state; i < matchers.Length; i++)
            {
                if (matchers[i].MatchEmpty() == MatchType.NO_MATCH)
                {
                    match = MatchType.NO_MATCH;
                    break;
                }
            }
            return match;
        }

        public void Reset()
        {
            foreach (IStringMatcher matcher in matchers)
            {
                matcher.Reset();
            }
            state = 0;
            hasMatched = false;
        }

        public MatchType Match(char c)
        {
            while (state < matchers.Length)
            {
                switch (matchers[state].Match(c))
                {
                    case MatchType.NO_MATCH:
                        if (!hasMatched && matchers[state].MatchEmpty() == MatchType.NO_MATCH)
                            return MatchType.NO_MATCH;
                        state++;
                        hasMatched = false;
                        break;
                    case MatchType.MATCH:
                        hasMatched = true;
                        return MatchRestEmpty(state + 1) == MatchType.MATCH ? MatchType.MATCH : MatchType.PARTIAL_MATCH;
                    case MatchType.PARTIAL_MATCH:
                        return MatchType.PARTIAL_MATCH;
                }
            }
            return MatchType.NO_MATCH;
        }

        public MatchType MatchEmpty()
        {
            return emptyMatch;
        }

        public IStringMatcher Clone()
        {
            IStringMatcher[] newMatchers = new IStringMatcher[matchers.Length];
            for (int i = 0; i < matchers.Length; i++)
            {
                newMatchers[i] = matchers[i].Clone();
            }
            return new GroupMatcher(newMatchers);
        }
    }

    public class DeepGroupMatcher : GroupMatcher
    {
        private LinkedList<IStringMatcher> recursiveMatchers;

        public DeepGroupMatcher(IStringMatcher[] matchers) : base(matchers)
        {
            recursiveMatchers = new LinkedList<IStringMatcher>();
        }

        public new MatchType Match(char c)
        {
            while (state < matchers.Length)
            {
                switch (matchers[state].Match(c))
                {
                    case MatchType.NO_MATCH:
                        if (!hasMatched && matchers[state].MatchEmpty() == MatchType.NO_MATCH)
                            return MatchType.NO_MATCH;
                        state++;
                        hasMatched = false;
                        break;
                    case MatchType.MATCH:
                        hasMatched = true;
                        return MatchRestEmpty(state + 1) == MatchType.MATCH ? MatchType.MATCH : MatchType.PARTIAL_MATCH;
                    case MatchType.PARTIAL_MATCH:
                        return MatchType.PARTIAL_MATCH;
                }
            }
            return MatchType.NO_MATCH;
        }

        public new IStringMatcher Clone()
        {
            IStringMatcher[] newMatchers = new IStringMatcher[matchers.Length];
            for (int i = 0; i < matchers.Length; i++)
            {
                newMatchers[i] = matchers[i].Clone();
            }
            return new GroupMatcher(newMatchers);
        }
    }

    public class ContainsMatcher : IStringMatcher
    {
        private readonly string matcheeString;
        private readonly LinkedList<char> matchee;
        private bool hasMatched;
        private readonly LinkedList<char> charList;

        public ContainsMatcher(string matchee)
        {
            this.matcheeString = matchee;
            this.matchee = ListifyString(matchee);
            this.hasMatched = false;
            this.charList = new LinkedList<char>();
        }

        private LinkedList<char> ListifyString(string s)
        {
            return new LinkedList<char>(s.ToCharArray());
        }

        public MatchType Match(char c)
        {
            if (hasMatched)
            {
                return MatchType.MATCH;
            }
            charList.AddLast(c);
            if (charList.Count == matchee.Count)
            {
                if (matchee.SequenceEqual(charList))
                {
                    hasMatched = true;
                    return MatchType.MATCH;
                }
                charList.RemoveFirst();
            }
            return MatchType.PARTIAL_MATCH;
        }

        public MatchType MatchEmpty()
        {
            return MatchType.NO_MATCH;
        }

        public void Reset()
        {
            hasMatched = false;
            charList.Clear();
        }

        public IStringMatcher Clone()
        {
            return new ContainsMatcher(matcheeString);
        }
    }

    public class NotContainsMatcher : IStringMatcher
    {
        private readonly string matcheeString;
        private readonly LinkedList<char> matchee;
        private bool hasMatched;
        private readonly LinkedList<char> charList;

        public NotContainsMatcher(string matchee)
        {
            this.matcheeString = matchee;
            this.matchee = ListifyString(matchee);
            this.hasMatched = false;
            this.charList = new LinkedList<char>();
        }

        private LinkedList<char> ListifyString(string s)
        {
            return new LinkedList<char>(s.ToCharArray());
        }

        public MatchType Match(char c)
        {
            if (hasMatched)
            {
                return MatchType.NO_MATCH;
            }
            charList.AddLast(c);
            if (charList.Count == matchee.Count)
            {
                if (matchee.SequenceEqual(charList))
                {
                    hasMatched = true;
                    return MatchType.NO_MATCH;
                }
                charList.RemoveFirst();
            }
            return MatchType.MATCH;
        }

        public MatchType MatchEmpty()
        {
            return MatchType.MATCH;
        }

        public void Reset()
        {
            hasMatched = false;
            charList.Clear();
        }

        public IStringMatcher Clone()
        {
            return new ContainsMatcher(matcheeString);
        }
    }

}