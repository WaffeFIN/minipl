﻿using System;
using System.Collections.Generic;
using System.Text;
using MiniPL;

namespace MiniPL.Lexing
{
    public class TokenRecognitionException : MiniPLException
    {
        /// <summary>
        /// Exception thrown when a token could not be recognized from the given input code.
        /// </summary>
        /// <param name="message"></param>
        public TokenRecognitionException(string message) : base(message)
        {
        }
    }
}
