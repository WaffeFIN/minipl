﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniPL.Lexing
{
    public class MiniPLToken : IToken<MiniPLTokenType>
    {
        private readonly MiniPLTokenType type;
        private readonly string tokenString;

        public MiniPLToken(MiniPLTokenType type, string tokenString)
        {
            this.type = type;
            this.tokenString = tokenString;
        }

        public string String
        {
            get { return tokenString; }
        }

        public MiniPLTokenType Type
        {
            get { return type; }
        }
    }

    public enum MiniPLTokenType
    {
        Identifier,
        Colon,
        DotDot,
        AssignmentOperator,
        PlusOperator,
        MinusOperator,
        TimesOperator,
        DivisionOperator,
        LessThanOperator,
        EqualsOperator,
        AndOperator,
        NotOperator,
        Semicolon,
        ParenthesisOpen,
        ParenthesisClose,
        IntegerValue,
        StringValue,
        Whitespace,
        CommentedLine,
        MultilineComment,
        Var,
        For,
        In,
        Do,
        End,
        Read,
        Print,
        Assert,
        IntType,
        StringType,
        BoolType,
        EOF
    }
}