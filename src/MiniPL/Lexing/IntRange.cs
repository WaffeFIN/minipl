﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniPL.Lexing
{
    class IntRange
    {
        private readonly int from;
        private readonly int toInclusive;
        
        internal IntRange(int from, int toInclusive) {
            this.from = from;
            this.toInclusive = toInclusive;
        }

        internal ISet<int> AsSet()
        {
            ISet<int> set = new HashSet<int>();
            for (int n = from; n <= toInclusive; n++) {
                set.Add(n);
            }
            return set;
        }
    }
}
