using System;

namespace MiniPL
{
    public enum ValueType
    {
        Unknown,
        String,
        Boolean,
        Integer
    }
}