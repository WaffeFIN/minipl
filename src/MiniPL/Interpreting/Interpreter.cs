﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using MiniPL.Lexing;
using MiniPL.Parsing;
using MiniPL.Traversing;
using MiniPL.Analyzing;

namespace MiniPL.Interpreting
{
    public class Interpreter<T>
    {
        private readonly IParser<T> parser;
        private readonly IAnalyzer<T> semanticAnalyzer;
        private readonly IInterpreter<T> interpreter;

        internal Interpreter(IParser<T> parser, IAnalyzer<T> semanticAnalyzer, IInterpreter<T> interpreter)
        {
            this.parser = parser;
            this.semanticAnalyzer = semanticAnalyzer;
            this.interpreter = interpreter;
        }

        public static Interpreter<MiniPLTokenType> MiniPL()
        {
            return new Interpreter<MiniPLTokenType>(
                new MiniPLParser(new MiniPLLexer()),
                new MiniPLAnalyzer(),
                new MiniPLInterpreter()
            );
        }

        public void Interpret(TextReader codeReader, TextReader input, TextWriter output)
        {

            IASTNode<T> rootNode = parser.Parse(codeReader);

            ISymbolTable<string, ValueType> symbolTable = new DefaultSymbolTable();
            semanticAnalyzer.VisitAll(symbolTable, new DepthFirstPreOrderTreeTraverser<IASTNode<T>>(rootNode));
            interpreter.VisitAll(symbolTable, new DepthFirstPreOrderTreeTraverser<IASTNode<T>>(rootNode), input, output);
        }
    }

    public class MiniPLInterpreter : IInterpreter<MiniPLTokenType>, IMiniPLVisitor
    {
        private ISymbolTable<string, ValueType> symbolTable;
        private TextReader input;
        private TextWriter output;

        private IDictionary<string, long> integerValueTable;
        private IDictionary<string, string> stringValueTable;
        private IDictionary<string, bool> booleanValueTable;

        private IInterpreterErrorHandler errorHandler;
        public IInterpreterErrorHandler ErrorHandler
        {
            [System.Diagnostics.DebuggerStepThrough]
            set { this.errorHandler = value; }
        }

        public void VisitAll(ISymbolTable<string, ValueType> symbolTable, ITreeTraverser<IASTNode<MiniPLTokenType>> traverser, TextReader input, TextWriter output)
        {
            this.symbolTable = symbolTable;
            this.input = input;
            this.output = output;

            integerValueTable = new Dictionary<string, long>();
            stringValueTable = new Dictionary<string, string>();
            booleanValueTable = new Dictionary<string, bool>();

            IASTNode<MiniPLTokenType> node;
            while ((node = traverser.Next()) != null)
            {
                ((IVisitable<IMiniPLVisitor>)node).Accept(this);
            }
        }

        public void Visit(ProgramNode node)
        {
        }

        public void Visit(StatementsNode node)
        {
        }

        public void Visit(StatementNode node)
        {
        }

        public void Visit(VariableDeclarationStatementNode node)
        {
            string identifier = node.GetIdentifier();
            if (node.HasExpression())
            {
                Assign(identifier, Evaluate(node.GetExpression()));
            }
        }

        public void Visit(VariableAssignmentStatementNode node)
        {
            string identifier = node.GetIdentifier();
            Assign(identifier, Evaluate(node.GetExpression()));
        }

        public void Visit(ForStatementNode node)
        {
            string identifier = node.GetIteratingIdentifier();
            long start = (long)Evaluate(node.GetRangeStartExpression());
            long end = (long)Evaluate(node.GetRangeEndExpression());

            Assign(identifier, start);
            for (long i = start; i < end; i++)
            {
                var traverser = new DepthFirstPreOrderTreeTraverser<IASTNode<MiniPLTokenType>>(node.GetStatements());

                IASTNode<MiniPLTokenType> recursiveNode;
                while ((recursiveNode = traverser.Next()) != null)
                {
                    ((IVisitable<IMiniPLVisitor>)recursiveNode).Accept(this);
                }
                Assign(identifier, i + 1);
            }
            //note that the i = end iteration is done by the VisitAll loop. Ugly but works for now...
        }

        public void Visit(EndForStatementNode node)
        {
        }

        public void Visit(ReadStatementNode node)
        {
            string inputLine = input.ReadLine();
            string identifier = node.GetIdentifier();
            ValueType type = symbolTable.TypeOf(identifier);
            switch (type)
            {
                case ValueType.Boolean:
                    Assign(identifier, ParseBooleanValue(inputLine));
                    break;
                case ValueType.Integer:
                    Assign(identifier, ParseIntegerValue(inputLine));
                    break;
                default:
                    Assign(identifier, inputLine);
                    break;
            }
        }

        private long ParseIntegerValue(string inputLine)
        {
            try
            {
                return Convert.ToInt64(inputLine);
            }
            catch (System.FormatException)
            {
                errorHandler.HandleInvalidValueException("integer", inputLine);
                return default(long);
            }
        }

        private bool ParseBooleanValue(string inputLine)
        {
            try
            {
                return Convert.ToBoolean(inputLine);
            }
            catch (System.FormatException)
            {
                errorHandler.HandleInvalidValueException("boolean", inputLine);
                return default(bool);
            }
        }

        private string ParseStringValue(string input)
        {
            return input.Substring(1, input.Length - 2) //remove quotes
                .Replace("\\\\", "\\")
                .Replace("\\\"", "\"")
                .Replace("\\n", "\n")
                .Replace("\\t", "\t")
                .Replace("\\b", "\b")
                .Replace("\\r", "\r")
                .Replace("\\f", "\f");
        }

        private void Assign(string identifier, object value)
        {
            switch (symbolTable.TypeOf(identifier))
            {
                case ValueType.Boolean:
                    Assign(identifier, (bool)value);
                    break;
                case ValueType.Integer:
                    Assign(identifier, (long)value);
                    break;
                default:
                    Assign(identifier, (string)value);
                    break;
            }

        }
        private void Assign(string identifier, long value)
        {
            integerValueTable[identifier] = value;
        }

        private void Assign(string identifier, bool value)
        {
            booleanValueTable[identifier] = value;
        }

        private void Assign(string identifier, string value)
        {
            stringValueTable[identifier] = value;
        }

        private object GetValue(string identifier)
        {
            switch (symbolTable.TypeOf(identifier))
            {
                case ValueType.Boolean:
                    return GetBooleanValue(identifier);
                case ValueType.Integer:
                    return GetIntegerValue(identifier);
                default:
                    return GetStringValue(identifier);
            }
        }

        private long GetIntegerValue(string identifier)
        {
            return integerValueTable[identifier];
        }

        private bool GetBooleanValue(string identifier)
        {
            return booleanValueTable[identifier];
        }

        private string GetStringValue(string identifier)
        {
            return stringValueTable[identifier];
        }

        public void Visit(PrintStatementNode node)
        {
            output.Write(Evaluate(node.GetExpression()));
        }

        public void Visit(AssertStatementNode node)
        {
        }

        public void Visit(ExpressionNode node)
        {
        }

        public void Visit(OperandNode node)
        {
        }

        public void Visit(OperatorNode node)
        {
        }

        public void Visit(TypeNode node)
        {
        }

        public void Visit(VariableIdentifierNode node)
        {
        }

        public void Visit(TokenNode node)
        {
        }

        private object Evaluate(ExpressionNode node)
        {
            if (!node.HasOperator())
                return Evaluate(node.GetFirstOperand());
            if (!node.HasSecondOperand())
                return Evaluate(node.GetOperator(), node.GetFirstOperand());
            return Evaluate(node.GetOperator(), node.GetFirstOperand(), node.GetSecondOperand());
        }

        private object Evaluate(OperatorNode op, OperandNode left, OperandNode right)
        {
            return op.Calculate(new object[] { Evaluate(left), Evaluate(right) });
        }

        private object Evaluate(OperatorNode op, OperandNode operand)
        {
            return op.Calculate(new object[] { Evaluate(operand) });
        }

        private object Evaluate(OperandNode node)
        {
            if (node.HasExpression())
            {
                return Evaluate(node.GetExpression());
            }
            else if (node.HasIdentifierNode())
            {
                return Evaluate(node.GetIdentifierNode());
            }
            else
            {
                return Evaluate(node.Type, node.Down().Token.String);
            }
        }

        private object Evaluate(VariableIdentifierNode node)
        {
            string identifier = node.GetIdentifier();
            return GetValue(identifier);
        }

        private object Evaluate(ValueType type, string value)
        {
            switch (type)
            {
                case ValueType.Boolean:
                    return ParseBooleanValue(value);
                case ValueType.Integer:
                    return ParseIntegerValue(value);
                default:
                    return ParseStringValue(value);
            }
        }
    }

    public class MiniPLInterpreterFactory
    {

        public Interpreter<MiniPLTokenType> Create()
        {
            var lexer = new MiniPLLexer();
            var parser = new MiniPLParser(lexer);
            var analyzer = new MiniPLAnalyzer();
            var interpreter = new MiniPLInterpreter();
            var errorHandler = new MiniPLErrorHandler();

            lexer.ErrorHandler = errorHandler;
            parser.ErrorHandler = errorHandler;
            analyzer.ErrorHandler = errorHandler;
            interpreter.ErrorHandler = errorHandler;

            return new Interpreter<MiniPLTokenType>(
                parser,
                analyzer,
                interpreter
            );
        }
    }
}
