﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiniPL
{
    /// <summary>
    /// An <c>Exception</c> class that all language-based exceptions should extend.
    /// </summary>
    public class MiniPLException : Exception
    {
        public MiniPLException(string message) : base(message) { }
    }
}
