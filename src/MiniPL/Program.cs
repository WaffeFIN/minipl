﻿using System;
using System.IO;
using MiniPL.Interpreting;

namespace MiniPL
{
    /// <summary>
    /// The main <c>Program</c> class.
    /// Starts an interpreter based on the Mini-PL specification from https://www.cs.helsinki.fi/u/vihavain/k18/Compilers/Project/Mini_pl_syntax_2018.pdf
    /// </summary>
    public class Program
    {
        public static readonly string OUTPUT_SEPARATOR = "]===========================[ ";

        private static void PressEnterToContinue()
        {
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();
        }

        public static void Main(string[] filenames)
        {
            if (filenames.Length == 0)
            {
                Console.WriteLine("Please enter the code file(s) as the program argument(s)");
            }
            for (int i = 0; i < filenames.Length; i++)
            {
                if (filenames.Length > 1)
                {
                    Console.WriteLine(OUTPUT_SEPARATOR + filenames[i] + ":");
                }
                try
                {
                    using (StreamReader codeReader = new StreamReader(filenames[i]))
                    {
                        new MiniPLInterpreterFactory().Create().Interpret(codeReader, Console.In, Console.Out);
                    }
                }
                catch (MiniPLException exception)
                {
                    Console.WriteLine(exception.Message);
                }
                catch (Exception exception)
                {
                    Console.WriteLine("Exception caught while reading " + filenames[i] + ":");
                    Console.WriteLine(exception.Message);
                }
            }
            PressEnterToContinue();
        }
    }
}
