﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MiniPL.Parsing;

namespace MiniPL
{
    /// <summary>
    /// Represents a lexer token with a token type T. In principle, T can be of any type, but an enum type is recommended.
    /// </summary>
    public interface IToken<T>
    {
        string String
        {
            get;
        }

        T Type
        {
            get;
        }
    }

    /// <summary>
    /// A lexer which reads the given TextReader and returns tokens with token type T.
    /// </summary>
    public interface ILexer<T>
    {
        bool HasNext();
        IToken<T> Next();

        void Init(TextReader reader);
        int Column
        {
            get;
        }
        int Row
        {
            get;
        }
    }

    /// <summary>
    /// A parser which reads the given tokens of type T and returns an AST root node.
    /// </summary>
    public interface IParser<T>
    {
        IASTNode<T> Parse(TextReader reader);
    }

    /// <summary>
    /// A generic ASTNode which extends ITreeNode<IASTNode<T>> while adding metadata used by the semantic analyzer.
    /// </summary>
    public interface IASTNode<T> : ITreeNode<IASTNode<T>>
    {
        IToken<T> Token
        {
            get; set;
        }
        int Column
        {
            get;
        }
        int Row
        {
            get;
        }
    }

    public interface IMiniPLASTNode : IASTNode<MiniPL.Lexing.MiniPLTokenType>, IVisitable<IMiniPLVisitor>
    {
    }

    /// <summary>
    /// A generic TreeNode with neighbors of the same ITreeNode implementation.
    /// </summary>
    public interface ITreeNode<T> where T : ITreeNode<T>
    {
        T Up();
        T Right();
        T Down();
        T DownRight();
        
        void Up(T node);
        void Right(T node);
        void Down(T node);
    }

    /// <summary>
    /// Traverses trees in the shape of ITreeNodes.
    /// </summary>
    public interface ITreeTraverser<T> where T : ITreeNode<T>
    {
        T Next();
    }

    /// <summary>
    /// Denotes that this class is visitable by V
    /// </summary>
    public interface IVisitable<V>
    {
        void Accept(V visitor);
    }

    public interface IMiniPLVisitor
    {
        void Visit(ProgramNode node);
        void Visit(StatementsNode node);
        void Visit(StatementNode node);
        void Visit(VariableDeclarationStatementNode node);
        void Visit(VariableAssignmentStatementNode node);
        void Visit(ForStatementNode node);
        void Visit(EndForStatementNode node);
        void Visit(ReadStatementNode node);
        void Visit(PrintStatementNode node);
        void Visit(AssertStatementNode node);
        void Visit(ExpressionNode node);
        void Visit(OperandNode node);
        void Visit(OperatorNode node);
        void Visit(TypeNode node);
        void Visit(VariableIdentifierNode node);
        void Visit(TokenNode node);
    }

    /// <summary>
    /// Analyzes the abstract syntax trees
    /// </summary>
    public interface IAnalyzer<T>
    {
        /// <summary>
        /// Analyzes the abstract syntax tree in the order determined by the given ITreeTraverser. Variable declarations are stored in the given symbolTable.
        /// </summary>
        void VisitAll(ISymbolTable<string, ValueType> symbolTable, ITreeTraverser<IASTNode<T>> traverser);
    }

    /// <summary>
    /// Operates upon given instructions
    /// </summary>
    public interface IInterpreter<T>
    {
        /// <summary>
        /// Operates upon given instructions in AST form. 
        /// </summary>
        void VisitAll(ISymbolTable<string, ValueType> symbolTable, ITreeTraverser<IASTNode<T>> traverser, TextReader input, TextWriter output);
    }

    public interface ISymbolTable<I, T>
    {
        void Declare(I identifier, T type);

        bool IsDeclared(I identifier);

        T TypeOf(I identifier);

        void EnterScope(); //not used in MiniPL but hey, why the hell not

        void LeaveScope();
    }
    
}
