﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MiniPL;
using MiniPL.Lexing;
using MiniPL.Parsing;
using MiniPL.Traversing;
using Xunit;

namespace MiniPLTester.Parsing
{
    public class LexerMock<T> : ILexer<T>
    {
        private readonly IToken<T>[] tokenStream;
        private readonly IToken<T> eof;
        private int index;

        internal LexerMock(IToken<T>[] tokenStream, IToken<T> eof) {
            this.tokenStream = tokenStream;
            this.eof = eof;
            this.index = 0;
        }

        public int Column => 0;
        public int Row => 0;

        public bool HasNext()
        {
            return index < tokenStream.Length;
        }

        public void Init(TextReader reader) {/*nothing*/}

        public IToken<T> Next()
        {
            return HasNext() ? tokenStream[index++] : eof;
        }
    }

    public class TestUtil {

        public int TreeSize<T> (T root) where T : TreeNode<T>
        {
            var traverser = new DepthFirstPreOrderTreeTraverser<T>(root);
            int size = 0;
            while (traverser.Next() != null) {
                size++;
            }
            return size;
        }
    }

    public class MiniPLParserTests
    {
        private static readonly IToken<MiniPLTokenType> eof = new MiniPLToken(MiniPLTokenType.EOF, "");

        [Fact]
        public void TestParsingSmall() {
            IToken<MiniPLTokenType>[] tokenStream = {
                new MiniPLToken(MiniPLTokenType.Var, "var"),
                new MiniPLToken(MiniPLTokenType.Identifier, "X"),
                new MiniPLToken(MiniPLTokenType.Colon, ":"),
                new MiniPLToken(MiniPLTokenType.StringType, "string"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";")
            };
            var parser = new MiniPLParser(new LexerMock<MiniPLTokenType>(tokenStream, eof))
            {
                ErrorHandler = new MiniPLErrorHandler()
            };
            IASTNode<MiniPLTokenType> rootNode = parser.Parse(null);

        }

        [Fact]
        public void TestParsingBig()
        {
            IToken<MiniPLTokenType>[] tokenStream = {
                new MiniPLToken(MiniPLTokenType.Var, "var"),
                new MiniPLToken(MiniPLTokenType.Identifier, "X"),
                new MiniPLToken(MiniPLTokenType.Colon, ":"),
                new MiniPLToken(MiniPLTokenType.StringType, "string"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";"),
                new MiniPLToken(MiniPLTokenType.For, "for"),
                new MiniPLToken(MiniPLTokenType.Identifier, "X"),
                new MiniPLToken(MiniPLTokenType.In, "in"),
                new MiniPLToken(MiniPLTokenType.ParenthesisOpen, "("),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "1"),
                new MiniPLToken(MiniPLTokenType.ParenthesisClose, ")"),
                new MiniPLToken(MiniPLTokenType.DotDot, ".."),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "08"),
                new MiniPLToken(MiniPLTokenType.PlusOperator, "+"),
                new MiniPLToken(MiniPLTokenType.Identifier, "X"),
                new MiniPLToken(MiniPLTokenType.Do, "do"),
                new MiniPLToken(MiniPLTokenType.Print, "print"),
                new MiniPLToken(MiniPLTokenType.StringValue, "\"Hello\""),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";"),
                new MiniPLToken(MiniPLTokenType.Print, "print"),
                new MiniPLToken(MiniPLTokenType.Identifier, "X"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";"),
                new MiniPLToken(MiniPLTokenType.End, "end"),
                new MiniPLToken(MiniPLTokenType.For, "for"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";"),
                new MiniPLToken(MiniPLTokenType.Assert, "assert"),
                new MiniPLToken(MiniPLTokenType.ParenthesisOpen, "("),
                new MiniPLToken(MiniPLTokenType.StringValue, "\"hi\""),
                new MiniPLToken(MiniPLTokenType.EqualsOperator, "="),
                new MiniPLToken(MiniPLTokenType.StringValue, "\"hi\""),
                new MiniPLToken(MiniPLTokenType.ParenthesisClose, ")"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";")
            };
            var parser = new MiniPLParser(new LexerMock<MiniPLTokenType>(tokenStream, eof))
            {
                ErrorHandler = new MiniPLErrorHandler()
            };
            IASTNode<MiniPLTokenType> rootNode = parser.Parse(null);
        }

        [Fact]
        public void TestParsingEmptyFail()
        {
            IToken<MiniPLTokenType>[] tokenStream = {
            };
            var parser = new MiniPLParser(new LexerMock<MiniPLTokenType>(tokenStream, eof))
            {
                ErrorHandler = new MiniPLErrorHandler()
            };
            Assert.Throws<ParseException>(() => parser.Parse(null));

        }

        [Fact]
        public void TestParsingVarSemicolonFail()
        {
            IToken<MiniPLTokenType>[] tokenStream = {
                new MiniPLToken(MiniPLTokenType.Var, "var"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";")
            };
            var parser = new MiniPLParser(new LexerMock<MiniPLTokenType>(tokenStream, eof))
            {
                ErrorHandler = new MiniPLErrorHandler()
            };
            Assert.Throws<ParseException>(() => parser.Parse(null));
        }

        [Fact]
        public void TestParsingExpressionFail1()
        {
            IToken<MiniPLTokenType>[] tokenStream = {
                new MiniPLToken(MiniPLTokenType.Print, "print"),
                new MiniPLToken(MiniPLTokenType.ParenthesisOpen, "("),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "123"),
                new MiniPLToken(MiniPLTokenType.AssignmentOperator, ":="),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "321"),
                new MiniPLToken(MiniPLTokenType.ParenthesisClose, ")"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";")
            };
            var parser = new MiniPLParser(new LexerMock<MiniPLTokenType>(tokenStream, eof))
            {
                ErrorHandler = new MiniPLErrorHandler()
            };
            Assert.Throws<ParseException>(() => parser.Parse(null));
        }

        [Fact]
        public void TestParsingExpressionFail2()
        {
            IToken<MiniPLTokenType>[] tokenStream = {
                new MiniPLToken(MiniPLTokenType.Print, "print"),
                new MiniPLToken(MiniPLTokenType.ParenthesisOpen, "("),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "123"),
                new MiniPLToken(MiniPLTokenType.PlusOperator, "+"),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "321"),
                new MiniPLToken(MiniPLTokenType.PlusOperator, "+"),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "213"),
                new MiniPLToken(MiniPLTokenType.ParenthesisClose, ")"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";")
            };
            var parser = new MiniPLParser(new LexerMock<MiniPLTokenType>(tokenStream, eof))
            {
                ErrorHandler = new MiniPLErrorHandler()
            };
            Assert.Throws<ParseException>(() => parser.Parse(null));
        }

        [Fact]
        public void TestEmptyForFail()
        {
            IToken<MiniPLTokenType>[] tokenStream = {
                new MiniPLToken(MiniPLTokenType.For, "for"),
                new MiniPLToken(MiniPLTokenType.Identifier, "X"),
                new MiniPLToken(MiniPLTokenType.In, "in"),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "1"),
                new MiniPLToken(MiniPLTokenType.DotDot, ".."),
                new MiniPLToken(MiniPLTokenType.IntegerValue, "08"),
                new MiniPLToken(MiniPLTokenType.Do, "do"),
                new MiniPLToken(MiniPLTokenType.End, "end"),
                new MiniPLToken(MiniPLTokenType.For, "for"),
                new MiniPLToken(MiniPLTokenType.Semicolon, ";")
            };
            var parser = new MiniPLParser(new LexerMock<MiniPLTokenType>(tokenStream, eof))
            {
                ErrorHandler = new MiniPLErrorHandler()
            };
            Assert.Throws<ParseException>(() => parser.Parse(null));
        }

    }
}
