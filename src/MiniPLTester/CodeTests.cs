using System;
using System.IO;
using Xunit;
using MiniPL.Lexing;
using MiniPL.Parsing;
using MiniPL.Analyzing;
using MiniPL.Interpreting;

namespace MiniPLTester
{
    public class LexerErrorCodeTests
    {
        [Fact]
        public void TestFaultyInteger()
        {
            string code;
            code = "var X : int := 123a321;";

            Assert.Throws<TokenRecognitionException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestUnterminatedString()
        {
            string code;
            code = "var X : string := \"asd;";

            Assert.Throws<TokenRecognitionException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestUnterminatedComment()
        {
            string code;
            code = "var i : int; /*for i in 1..10 do print i; end for;";

            Assert.Throws<TokenRecognitionException>(() => TestUtil.Interpret(code, ""));
        }
    }

    public class ParserErrorCodeTests
    {
        [Fact]
        public void TestEmpty()
        {
            string code;
            code = "";

            Assert.Throws<ParseException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestUnterminatedFor()
        {
            string code;
            code = "var i : int; for i in 1..10 do print i;";

            Assert.Throws<ParseException>(() => TestUtil.Interpret(code, ""));
        }
    }

    public partial class AnalyzerErrorCodeTests
    {
        //Type errors
        [Fact]
        public void TestAssignIntegerValueToStringType()
        {
            string code;
            code = "var a : string;\na := 10;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestAssignStringValueToIntegerType()
        {
            string code;
            code = "var a : int;\na := \"HEY!\";";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestAssignBooleanValueToStringType()
        {
            string code;
            code = "var a : string;\na := (\"hi\" = \"hi\");";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestAssignBooleanValueToIntegerType()
        {
            string code;
            code = "var a : int;\na := (123 = 321);";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestAssignIntegerValueToBooleanType()
        {
            string code;
            code = "var a : bool;\na := 10;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestAssignStringValueToBooleanType()
        {
            string code;
            code = "var a : bool;\na := \"\";";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestPlusExpressionWithStringValueAndIntegerValue()
        {
            string code;
            code = "print \"hello\" + 123;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestPlusExpressionWithIntegerValueAndStringValue()
        {
            string code;
            code = "print (123 + \"hello\");";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestNestedExpressionTypeChecks1()
        {
            string code;
            code = "print (123 + (\"hello\"));";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestNestedExpressionTypeChecks2()
        {
            string code;
            code = "print (\"test\" + (123 + 321));";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestForLoopWithStringValue()
        {
            string code;
            code = "var s : string := \"9\"; \nfor s in 1..10 do print \"\" ; end for;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestForLoopWithStringValueInRangeStart()
        {
            string code;
            code = "var s : string := \"1\"; var X : int; \nfor X in s..7 do print \"\" ; end for;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestForLoopWithStringValueInRangeEnd()
        {
            string code;
            code = "var s : string := \"8\"; var X : int; \nfor X in 1..s do print \"\" ; end for;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestAssertStringValue()
        {
            string code;
            code = "var s : string := \"8\"; assert ( s );";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestAssertIntegerValue()
        {
            string code;
            code = "var i : int := 1; assert ( i );";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestBooleanOperatorOnIntegerValue()
        {
            string code;
            code = "var i : int := 1; print i & 2;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestBooleanOperatorOnStringValue()
        {
            string code;
            code = "var s : string := \"str\"; print !s;";

            Assert.Throws<TypeException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestReadStringValueToIntegerVariable()
        {
            string code;
            code = "var i : int; read i; print i;";

            Assert.Throws<InvalidValueException>(() => TestUtil.Interpret(code, "hello\n"));
        }
    }

    public partial class AnalyzerErrorCodeTests
    {
        //Other static errors
        [Fact]
        public void TestUndeclaredVariable()
        {
            string code;
            code = "print s;";

            Assert.Throws<UndeclaredVariableException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestUndeclaredVariableInFor()
        {
            string code;
            code = "for s in 1..10 do print \"\"; end for;";

            Assert.Throws<UndeclaredVariableException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestRedeclaringVariable()
        {
            string code;
            code = "var s : string; var s : string := \"\";";

            Assert.Throws<DuplicateDeclarationException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestModifyingForControlVariableWithAssignment()
        {
            string code;
            code = "var CV : int; for CV in 1..10 do print \"\"; CV := CV + 1; end for; print CV;";

            Assert.Throws<IllegalModificationException>(() => TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestModifyingForControlVariableWithRead()
        {
            string code;
            code = "var CV : int; for CV in 1..10 do print \"\"; read CV; end for; print CV;";

            Assert.Throws<IllegalModificationException>(() => TestUtil.Interpret(code, "10\n"));
        }

        [Fact]
        public void TestModifyingNestedForControlVariableWithAssignment()
        {
            string code;
            code = "var X : int := 6; var Y : int; var Z : int := X; \n"
                + "for X in 1..10 do for Y in 1..X do Z := 1; X := Z; end for; end for; print Z;";

            Assert.Throws<IllegalModificationException>(() => TestUtil.Interpret(code, ""));
        }

        [Fact]
        public void TestModifyingNestedForControlVariableWithRead()
        {
            string code;
            code = "var X : int := 6; var Y : int; var Z : int := X; \n"
                + "for X in 1..10 do for Y in 1..X do read Z; read X; end for; end for; print Z;";

            Assert.Throws<IllegalModificationException>(() => TestUtil.Interpret(code, "10\n6\n"));
        }
    }

    public class CompilableCodeTests
    {
        [Fact]
        public void TestPrintHi()
        {
            string code;
            code = "\n\nprint \"hi\\n\"\t;";

            Assert.Equal("hi\n", TestUtil.Interpret(code, ""));
        }

        [Fact]
        public void TestPrintHiWithComments()
        {
            string code;
            code = "/* This is a \"hi\" printer */ print \"hi \"; // TODO: extend with hello functionality\n print \"good bye!\";";

            Assert.Equal("hi good bye!", TestUtil.Interpret(code, ""));
        }

        [Fact]
        public void TestPrintHiWithCommentsWithAsterisk()
        {
            string code;
            code = "/* This should be commented: * out */ print \"hi\";";

            Assert.Equal("hi", TestUtil.Interpret(code, ""));
        }

        [Theory]
        [InlineData("int", "0")]
        [InlineData("int", "918723")]
        [InlineData("int", "-123")]
        [InlineData("string", "10")]
        [InlineData("string", "hello")]
        public void TestReadAndPrint(string type, string input)
        {
            string code;
            code = "var X : " + type + "; read X; print X;";

            Assert.Equal(input, TestUtil.Interpret(code, input));
        }

        [Fact]
        public void TestReadAndPrintWithEscapeChar()
        {
            string code;
            code = "var X : string; read X; print X;";

            string input="This is a haiku\\nWhich should continue alone\\nOn separate row";
            Assert.Equal("This is a haiku\nWhich should continue alone\nOn separate row", TestUtil.Interpret(code, input));
        }

        [Fact]
        public void TestAssignmentAndPrint()
        {
            string code;
            code = "var X : int := 4 + (6 * 2);\n"
                + "print X;";

            Assert.Equal("16", TestUtil.Interpret(code, ""));
        }

        [Fact]
        public void TestFactorial1()
        {
            string code, userInput;
            code = "print \"Give a number\";\r\n"
                + "var n : int;\r\n"
                + "read n;\r\n"
                + "var v : int := 1;\r\n"
                + "var i : int;\r\n"
                + "for i in 1..n do\r\n"
                + "v := v * i;\r\n"
                + "end for;\r\n"
                + "print \"The result is: \";\r\n"
                + "print v; ";

            userInput = "3\n";

            Assert.Equal("Give a numberThe result is: 6", TestUtil.Interpret(code, userInput));
        }

        [Fact]
        public void TestFactorial2()
        {
            string code, userInput;
            code = "print \"Give a number\";\r\n"
                + "var n : int;\r\n"
                + "read n;\r\n"
                + "var v : int := 1;\r\n"
                + "var i : int;\r\n"
                + "for i in 1..n do\r\n"
                + "v := v * i;\r\n"
                + "end for;\r\n"
                + "print \"The result is: \";\r\n"
                + "print v; ";

            userInput = "7\n";

            Assert.Equal("Give a numberThe result is: 5040", TestUtil.Interpret(code, userInput));
        }

        [Fact]
        public void TestMultiplePrints()
        {
            string code, userInput;
            code = "var nTimes : int := 0;\r\n"
                + "print \"How many times ? \";\r\n"
                + "read nTimes;\r\n"
                + "var x : int;\r\n"
                + "for x in 0..nTimes-1 do\r\n"
                + "print x;\r\n"
                + "print \" : Hello, World!\\n\";\r\n"
                + "end for;\r\n"
                + "assert (x = nTimes);\r\n";

            userInput = "5\n";

            Assert.Equal("How many times ? 0 : Hello, World!\n"
                + "1 : Hello, World!\n"
                + "2 : Hello, World!\n"
                + "3 : Hello, World!\n"
                + "4 : Hello, World!\n", TestUtil.Interpret(code, userInput)); //TODO: Add assertion failed?
        }

        [Fact]
        public void TestNestedExpressionTypeChecks3()
        {
            string code;
            code = "var hello : string := \"hello\"; print (\"hello\"+(\" \" + hello));";

            Assert.Equal("hello hello", TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestNestedExpressionTypeChecks4()
        {
            string code;
            code = "var num : int := 123; print ((1 + 2)+ 3) * (num*(12 + 32));";

            Assert.Equal("32472", TestUtil.Interpret(code, ""));
        }
        [Fact]
        public void TestReadIntegerValueToStringVariable()
        {
            string code;
            code = "var s : string; read s; print s;";

            Assert.Equal("87123", TestUtil.Interpret(code, "87123\n"));
        }

        [Fact]
        public void TestNestedForLoops()
        {
            string code;
            code = "var X : int := 6; var Y : int; var Z : int := X; \n"
                + "for X in 1..10 do for Y in 1..X do Z := Z + 1; end for; end for; print Z;";

            Assert.Equal("61", TestUtil.Interpret(code, ""));
        }

        [Fact]
        public void TestAll()
        {
        }
    }

    public class TestUtil
    {

        public static string Interpret(string code, string input)
        {
            TextWriter output = new StringWriter();
            new MiniPLInterpreterFactory().Create().Interpret(new StringReader(code), new StringReader(input), output);

            return output.ToString();
        }
    }
}
