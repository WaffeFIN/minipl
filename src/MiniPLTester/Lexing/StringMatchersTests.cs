using System;
using System.Collections.Generic;
using System.Text;
using MiniPL.Lexing;
using Xunit;


namespace MiniPLTester.Lexing
{
    public class TestUtil
    {
        public void TestMatcher(IStringMatcher matcher, string s, MatchType expectedResult)
        {
            matcher.Reset();
            for (int i = 0; i < s.Length - 1; i++)
            {
                matcher.Match(s[i]);
            }
            Assert.Equal(expectedResult, matcher.Match(s[s.Length - 1]));
        }
    }

    public class EqualityMatcherTests
    {
        [Theory]
        [InlineData("", MatchType.MATCH)]
        [InlineData("a", MatchType.NO_MATCH)]
        [InlineData("anything", MatchType.NO_MATCH)]
        [InlineData("0123", MatchType.NO_MATCH)]
        public void TestMatchEmpty(string matchee, MatchType expectedResult)
        {
            Assert.Equal(expectedResult, new EqualityMatcher(matchee).MatchEmpty());
        }

        [Theory]
        [InlineData("a", "a", MatchType.MATCH)]
        [InlineData("asdf", "asdf", MatchType.MATCH)]
        [InlineData("a", "s", MatchType.NO_MATCH)]
        [InlineData("asdf", "axcv", MatchType.NO_MATCH)]
        [InlineData("asdf", "axdf", MatchType.NO_MATCH)]
        [InlineData("tHiNg", "thing", MatchType.NO_MATCH)]
        [InlineData("thing", "tHiNg", MatchType.NO_MATCH)]
        [InlineData("this is a Story", "this is a Story", MatchType.MATCH)]
        [InlineData("aa", "a", MatchType.PARTIAL_MATCH)]
        [InlineData("this is a Story", "this is a S", MatchType.PARTIAL_MATCH)]
        public void TestMatch(string matchee, string input, MatchType expectedResult)
        {
            new TestUtil().TestMatcher(new EqualityMatcher(matchee), input, expectedResult);
        }

        [Theory]
        [InlineData("a", "a", MatchType.MATCH, "s", MatchType.NO_MATCH, "a", MatchType.MATCH)]
        [InlineData("aa", "a", MatchType.PARTIAL_MATCH, "a", MatchType.PARTIAL_MATCH, "aa", MatchType.MATCH)]
        [InlineData("this is a Story", "this", MatchType.PARTIAL_MATCH, " is", MatchType.NO_MATCH, "this is a Story", MatchType.MATCH)]
        public void TestMatchAndReset(string matchee, string input1, MatchType expectedResult1, string input2, MatchType expectedResult2, string input3, MatchType expectedResult3)
        {
            var matcher = new EqualityMatcher(matchee);
            var tester = new TestUtil();
            tester.TestMatcher(matcher, input1, expectedResult1);
            tester.TestMatcher(matcher, input2, expectedResult2);
            tester.TestMatcher(matcher, input3, expectedResult3);
        }
    }

    public class RestrictedAlphabetMatcherTests
    {
        [Fact]
        public void TestMatchEmpty()
        {
            Assert.Equal(MatchType.MATCH, new RestrictedAlphabetMatcher(new HashSet<int>(new int[] { 0, 2, 4 })).MatchEmpty());
            Assert.Equal(MatchType.MATCH, new RestrictedAlphabetMatcher(new HashSet<int>()).MatchEmpty());
        }

        [Theory]
        [InlineData(new int[] { 'a' }, "aaa", MatchType.MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "abbaaab", MatchType.MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "c", MatchType.NO_MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "abcab", MatchType.NO_MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "aabbAABBab", MatchType.NO_MATCH)]
        [InlineData(new int[] { 'A', 'B', ' ' }, "A ABBA BAAB", MatchType.MATCH)]
        public void TestMatch(int[] characters, string input, MatchType expectedType)
        {
            new TestUtil().TestMatcher(new RestrictedAlphabetMatcher(new HashSet<int>(characters)), input, expectedType);
        }
    }

    public class NonEmptyRestrictedAlphabetMatcherTests
    {
        [Fact]
        public void TestMatchEmpty()
        {
            Assert.Equal(MatchType.NO_MATCH, new NonEmptyRestrictedAlphabetMatcher(new HashSet<int>(new int[] { 0, 2, 4 })).MatchEmpty());
            Assert.Equal(MatchType.NO_MATCH, new NonEmptyRestrictedAlphabetMatcher(new HashSet<int>()).MatchEmpty());
        }

        [Theory]
        [InlineData(new int[] { 'a' }, "aaa", MatchType.MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "abbaaab", MatchType.MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "c", MatchType.NO_MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "abcab", MatchType.NO_MATCH)]
        [InlineData(new int[] { 'a', 'b' }, "aabbAABBab", MatchType.NO_MATCH)]
        [InlineData(new int[] { 'A', 'B', ' ' }, "A ABBA BAAB", MatchType.MATCH)]
        public void TestMatch(int[] characters, string input, MatchType expectedType)
        {
            new TestUtil().TestMatcher(new NonEmptyRestrictedAlphabetMatcher(new HashSet<int>(characters)), input, expectedType);
        }

        [Theory]
        [InlineData(new int[] { 'a' }, new int[] { 'b' }, "aaaa", MatchType.PARTIAL_MATCH)]
        [InlineData(new int[] { 'a' }, new int[] { 'b' }, "b", MatchType.NO_MATCH)]
        [InlineData(new int[] { 'a' }, new int[] { 'b' }, "ab", MatchType.MATCH)]
        [InlineData(new int[] { 'a' }, new int[] { 'b' }, "aba", MatchType.NO_MATCH)]
        public void TestMatchWithGroupMatcher(int[] characters1, int[] characters2, string input, MatchType expectedType)
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new NonEmptyRestrictedAlphabetMatcher(new HashSet<int>(characters1)),
                new NonEmptyRestrictedAlphabetMatcher(new HashSet<int>(characters2)),
            };
            new TestUtil().TestMatcher(new GroupMatcher(matchers), input, expectedType);
        }
    }

    public class RestrictedAlphabetWithSubstringsMatcherTests
    {
        [Fact]
        public void TestMatchEmpty1()
        {
            IStringMatcher matcher = new RestrictedAlphabetWithSubstringsMatcher(
                new RestrictedAlphabetMatcher(new HashSet<int>() { 'h', 'i', 's', 't', 'r', 'n', 'g', ' ' }),
                        new IStringMatcher[]{
                            new EqualityMatcher("\\\\"),
                            new EqualityMatcher("\\\""),
                            new EqualityMatcher("\\n"),
                            new EqualityMatcher("\\t"),
                            new EqualityMatcher("\\b"),
                            new EqualityMatcher("\\r"),
                            new EqualityMatcher("\\f"),
                        });
            Assert.Equal(MatchType.MATCH, matcher.MatchEmpty());
        }

        [Fact]
        public void TestMatchEmpty2()
        {
            IStringMatcher matcher = new RestrictedAlphabetWithSubstringsMatcher(
                new RestrictedAlphabetMatcher(new HashSet<int>() { 'h', 'i', 's', 't', 'r', 'n', 'g', ' ' }),
                        new IStringMatcher[]{
                            new EqualityMatcher("x")
                        });
            matcher.Match('x');
            Assert.Equal(MatchType.MATCH, matcher.MatchEmpty());
        }

    [Theory]
        [InlineData("this is string", MatchType.MATCH)]
        [InlineData("thing \\\\ is", MatchType.MATCH)]
        [InlineData("thing \\\" is", MatchType.MATCH)]
        [InlineData("thing \\n is", MatchType.MATCH)]
        [InlineData("thing \\n", MatchType.MATCH)]
        [InlineData("\\n", MatchType.MATCH)]
        [InlineData("thing \\t is", MatchType.MATCH)]
        [InlineData("thing \\b is", MatchType.MATCH)]
        [InlineData("thing \\r is", MatchType.MATCH)]
        [InlineData("thing \\f is", MatchType.MATCH)]
        [InlineData("sing his thing \\b\\n\\t\\r\\f\\\\\\\" sings ", MatchType.MATCH)]
        [InlineData("this is nt \\ ttt", MatchType.NO_MATCH)]
        [InlineData("\\\\\\", MatchType.PARTIAL_MATCH)]
        public void TestMatchStrings(string input, MatchType expectedType)
        {
            IStringMatcher matcher = new RestrictedAlphabetWithSubstringsMatcher(
                new RestrictedAlphabetMatcher(new HashSet<int>() { 'h', 'i', 's', 't', 'r', 'n', 'g', ' ' }),
                        new IStringMatcher[]{
                            new EqualityMatcher("\\\\"),
                            new EqualityMatcher("\\\""),
                            new EqualityMatcher("\\n"),
                            new EqualityMatcher("\\t"),
                            new EqualityMatcher("\\b"),
                            new EqualityMatcher("\\r"),
                            new EqualityMatcher("\\f"),
                        });
            new TestUtil().TestMatcher(matcher, input, expectedType);
        }
    }

    public class GroupMatcherTests
    {
        [Fact]
        public void TestGroupMatcherSimple1()
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new EqualityMatcher("a")
            };
            new TestUtil().TestMatcher(new GroupMatcher(matchers), "a", MatchType.MATCH);
        }

        [Fact]
        public void TestGroupMatcherSimple2()
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new EqualityMatcher("a"),
                new EqualityMatcher("a")
            };
            new TestUtil().TestMatcher(new GroupMatcher(matchers), "aa", MatchType.MATCH);
        }

        [Theory]
        [InlineData("baa", MatchType.NO_MATCH)]
        [InlineData("b", MatchType.PARTIAL_MATCH)]
        [InlineData("ba", MatchType.PARTIAL_MATCH)]
        [InlineData("bab", MatchType.MATCH)]
        [InlineData("abab", MatchType.NO_MATCH)]
        public void TestGroupMatcherSimple3(string input, MatchType expectedType)
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new EqualityMatcher("b"),
                new EqualityMatcher("a"),
                new EqualityMatcher("b")
            };
            var matcher = new GroupMatcher(matchers);
            new TestUtil().TestMatcher(matcher, input, expectedType);
        }

        [Theory]
        [InlineData("cc", MatchType.MATCH)]
        [InlineData("cabc", MatchType.MATCH)]
        [InlineData("cabababaaaabbabababc", MatchType.MATCH)]
        [InlineData("c", MatchType.PARTIAL_MATCH)]
        [InlineData("cba", MatchType.PARTIAL_MATCH)]
        [InlineData("casd", MatchType.NO_MATCH)]
        [InlineData("acaac", MatchType.NO_MATCH)]
        public void TestGroupMatcher1(string input, MatchType expectedType)
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new EqualityMatcher("c"),
                new RestrictedAlphabetMatcher(new HashSet<int>(new int[]{'a', 'b'})),
                new EqualityMatcher("c")
            };
            var matcher = new GroupMatcher(matchers);
            new TestUtil().TestMatcher(matcher, input, expectedType);
        }

        [Theory]
        [InlineData("a", MatchType.MATCH)]
        [InlineData("abba", MatchType.MATCH)]
        [InlineData("abbc", MatchType.MATCH)]
        [InlineData("bcbcbc", MatchType.MATCH)]
        [InlineData("c", MatchType.MATCH)]
        [InlineData("ababbababcbcbcbbcc", MatchType.MATCH)]
        [InlineData("abbcbcba", MatchType.NO_MATCH)]
        [InlineData("x", MatchType.NO_MATCH)]
        public void TestGroupMatcher2(string input, MatchType expectedType)
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new RestrictedAlphabetMatcher(new HashSet<int>(new int[]{'a', 'b'})),
                new RestrictedAlphabetMatcher(new HashSet<int>(new int[]{'b', 'c'}))
            };
            var matcher = new GroupMatcher(matchers);
            new TestUtil().TestMatcher(matcher, input, expectedType);
        }

        [Theory]
        [InlineData("/*/", MatchType.NO_MATCH)]
        [InlineData("/**", MatchType.PARTIAL_MATCH)]
        [InlineData("/**/", MatchType.MATCH)]
        [InlineData("/***/", MatchType.MATCH)]
        public void TestGroupMatcherWithAsterisks(string input, MatchType expectedType)
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new EqualityMatcher("/*"),
                new RestrictedAlphabetMatcher(new HashSet<int>(new int[]{'*'})),
                new EqualityMatcher("*/")
            };
            var matcher = new GroupMatcher(matchers);
            new TestUtil().TestMatcher(matcher, input, expectedType);
        }
    }

    public class DeepGroupMatcherTests
    {
        [Theory]
        [InlineData("(end)", MatchType.MATCH)]
        [InlineData("(abend)", MatchType.MATCH)]
        [InlineData("(abababaaaabbabababend)", MatchType.MATCH)]
        [InlineData("(", MatchType.PARTIAL_MATCH)]
        [InlineData("(ba", MatchType.PARTIAL_MATCH)]
        [InlineData("(abend", MatchType.PARTIAL_MATCH)]
        [InlineData("(asd", MatchType.NO_MATCH)]
        [InlineData("a(aa)", MatchType.NO_MATCH)]
        [InlineData(")", MatchType.NO_MATCH)]
        [InlineData("(end())", MatchType.MATCH)]
        [InlineData("(()end)", MatchType.MATCH)]
        [InlineData("(ab(abend)end)", MatchType.MATCH)]
        [InlineData("(abend(abend))", MatchType.MATCH)]
        [InlineData("(abend(abend)end)", MatchType.NO_MATCH)]
        [InlineData("()", MatchType.NO_MATCH)]
        [InlineData("((end))", MatchType.NO_MATCH)]
        [InlineData("(()end)", MatchType.NO_MATCH)]
        public void TestDeepGroupMatcher(string input, MatchType expectedType)
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new EqualityMatcher("("),
                new RestrictedAlphabetMatcher(new HashSet<int>(new int[]{'a', 'b'})),
                new EqualityMatcher("end"),
                new EqualityMatcher(")")
            };
            var matcher = new DeepGroupMatcher(matchers);
            new TestUtil().TestMatcher(matcher, input, expectedType);
        }

        [Theory]
        [InlineData("text", MatchType.NO_MATCH)]
        [InlineData("/*", MatchType.PARTIAL_MATCH)]
        [InlineData("/*text", MatchType.PARTIAL_MATCH)]
        [InlineData("/**/", MatchType.MATCH)]
        [InlineData("/*/*", MatchType.PARTIAL_MATCH)]
        [InlineData("/*/*/*/**/*/", MatchType.PARTIAL_MATCH)]
        [InlineData("/*/*/*/**/*/*/*/", MatchType.MATCH)]
        [InlineData("/*/*/*/*text*/*/*/*/", MatchType.MATCH)]
        [InlineData("/*/*/*text/*text*/*/*/*/", MatchType.MATCH)]
        [InlineData("/*/*/*/*text*/text*/*/*/", MatchType.MATCH)]
        [InlineData("/*text/*text/*text/*text*/text*/text*/text*/", MatchType.MATCH)]
        [InlineData("/**/*/", MatchType.NO_MATCH)]
        [InlineData("/**/text", MatchType.NO_MATCH)]
        [InlineData("/**//**/", MatchType.NO_MATCH)]
        public void TestDeepGroupMatcherWithComments(string input, MatchType expectedType)
        {
            IStringMatcher[] matchers = new IStringMatcher[] {
                new EqualityMatcher("/*"),
                new RestrictedAlphabetMatcher(new HashSet<int>(new int[]{'t', 'e', 'x'})),
                new EqualityMatcher("*/")
            };
            var matcher = new DeepGroupMatcher(matchers);
            new TestUtil().TestMatcher(matcher, input, expectedType);
        }
    }

    public class ContainsMatcherTests
    {
        [Theory]
        [InlineData("", MatchType.NO_MATCH)]
        [InlineData("a", MatchType.NO_MATCH)]
        public void TestMatchEmpty(string matchee, MatchType expectedResult)
        {
            Assert.Equal(expectedResult, new ContainsMatcher(matchee).MatchEmpty());
        }

        [Theory]
        [InlineData("a", "a", MatchType.MATCH)]
        [InlineData("asdf", "asdf", MatchType.MATCH)]
        [InlineData("a", "s", MatchType.PARTIAL_MATCH)]
        [InlineData("asdf", "axcv", MatchType.PARTIAL_MATCH)]
        [InlineData("asdf", "axdf", MatchType.PARTIAL_MATCH)]
        [InlineData("tHiNg", "thing", MatchType.PARTIAL_MATCH)]
        [InlineData("thing", "tHiNg", MatchType.PARTIAL_MATCH)]
        [InlineData("this is a Story", "this is a Story", MatchType.MATCH)]
        [InlineData("aa", "a", MatchType.PARTIAL_MATCH)]
        [InlineData("this is a Story", "this is a S", MatchType.PARTIAL_MATCH)]
        [InlineData("a", "aa", MatchType.MATCH)]
        [InlineData("this is a S", "Hello, this is a Story", MatchType.MATCH)]
        public void TestMatch(string matchee, string input, MatchType expectedResult)
        {
            new TestUtil().TestMatcher(new ContainsMatcher(matchee), input, expectedResult);
        }
    }
    public class NotContainsMatcherTests
    {
        [Theory]
        [InlineData("", MatchType.MATCH)]
        [InlineData("a", MatchType.MATCH)]
        public void TestMatchEmpty(string matchee, MatchType expectedResult)
        {
            Assert.Equal(expectedResult, new NotContainsMatcher(matchee).MatchEmpty());
        }

        [Theory]
        [InlineData("a", "a", MatchType.NO_MATCH)]
        [InlineData("asdf", "asdf", MatchType.NO_MATCH)]
        [InlineData("a", "s", MatchType.MATCH)]
        [InlineData("asdf", "axcv", MatchType.MATCH)]
        [InlineData("asdf", "axdf", MatchType.MATCH)]
        [InlineData("tHiNg", "thing", MatchType.MATCH)]
        [InlineData("thing", "tHiNg", MatchType.MATCH)]
        [InlineData("this is a Story", "this is a Story", MatchType.NO_MATCH)]
        [InlineData("aa", "a", MatchType.MATCH)]
        [InlineData("this is a Story", "this is a S", MatchType.MATCH)]
        public void TestMatch(string matchee, string input, MatchType expectedResult)
        {
            new TestUtil().TestMatcher(new NotContainsMatcher(matchee), input, expectedResult);
        }
    }
}