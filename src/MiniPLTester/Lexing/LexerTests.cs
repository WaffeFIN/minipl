using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MiniPL;
using MiniPL.Lexing;
using Xunit;

namespace MiniPLTester.Lexing
{
    public class TestUtil<T>
    {
        public void TestTokens(ILexer<T> lexer, string s, T[] expectedResults)
        {
            lexer.Init(new StringReader(s));
            for (int i = 0; i < expectedResults.Length; i++)
            {
                IToken<T> token = lexer.Next();
                Assert.Equal(expectedResults[i], token.Type);
            }
        }

        public void TestPositions(ILexer<T> lexer, string s, int[] expectedRows, int[] expectedColumns)
        {
            lexer.Init(new StringReader(s));
            for (int i = 0; i < expectedRows.Length; i++)
            {
                Assert.Equal(expectedRows[i] * 10000 + expectedColumns[i], lexer.Row * 10000 + lexer.Column);
                IToken<T> token = lexer.Next();
            }
        }
    }

    public class MiniPLLexerTests
    {
        [Theory]
        [InlineData("variable", new MiniPLTokenType[] {MiniPLTokenType.Identifier })]
        [InlineData("Print", new MiniPLTokenType[] { MiniPLTokenType.Identifier })]
        [InlineData("endfor", new MiniPLTokenType[] { MiniPLTokenType.Identifier })]
        [InlineData("an_0123456789_Identifier_the_quick_brown_fox_jumped_over_the_lazy_dog_THE_QUICK_BROWN_FOX_JUMPED_OVER_THE_LAZY_DOG", new MiniPLTokenType[] { MiniPLTokenType.Identifier, MiniPLTokenType.EOF })]
        [InlineData(":", new MiniPLTokenType[] {MiniPLTokenType.Colon })]
        [InlineData("..", new MiniPLTokenType[] {MiniPLTokenType.DotDot, MiniPLTokenType.EOF})]
        [InlineData(":=", new MiniPLTokenType[] {MiniPLTokenType.AssignmentOperator })]
        [InlineData("+", new MiniPLTokenType[] {MiniPLTokenType.PlusOperator })]
        [InlineData("-", new MiniPLTokenType[] {MiniPLTokenType.MinusOperator })]
        [InlineData("*", new MiniPLTokenType[] {MiniPLTokenType.TimesOperator })]
        [InlineData(" / ", new MiniPLTokenType[] {MiniPLTokenType.DivisionOperator, MiniPLTokenType.EOF})]
        [InlineData("<", new MiniPLTokenType[] {MiniPLTokenType.LessThanOperator })]
        [InlineData("=", new MiniPLTokenType[] {MiniPLTokenType.EqualsOperator })]
        [InlineData("& ", new MiniPLTokenType[] {MiniPLTokenType.AndOperator })]
        [InlineData(" !", new MiniPLTokenType[] {MiniPLTokenType.NotOperator })]
        [InlineData(";", new MiniPLTokenType[] {MiniPLTokenType.Semicolon })]
        [InlineData("( ", new MiniPLTokenType[] {MiniPLTokenType.ParenthesisOpen })]
        [InlineData(" )", new MiniPLTokenType[] {MiniPLTokenType.ParenthesisClose })]
        [InlineData("0123", new MiniPLTokenType[] {MiniPLTokenType.IntegerValue })]
        [InlineData("9812637812310", new MiniPLTokenType[] { MiniPLTokenType.IntegerValue })]
        [InlineData("/*/ */0", new MiniPLTokenType[] { MiniPLTokenType.IntegerValue })]
        [InlineData("\"var\"", new MiniPLTokenType[] {MiniPLTokenType.StringValue })]
        [InlineData("\"/*var*/\"", new MiniPLTokenType[] { MiniPLTokenType.StringValue })]
        [InlineData("/*\"var\"*/ in", new MiniPLTokenType[] { MiniPLTokenType.In })]
        [InlineData("  \t\r\n  \t var", new MiniPLTokenType[] {MiniPLTokenType.Var })]
        [InlineData("//var this is comment\nfor", new MiniPLTokenType[] {MiniPLTokenType.For })]
        [InlineData("/*yeah this is multiline comment*/ in", new MiniPLTokenType[] {MiniPLTokenType.In })]
        [InlineData("/*nested /* multiline */ \t COMMENT */", new MiniPLTokenType[] {MiniPLTokenType.Do })]
        [InlineData("end for", new MiniPLTokenType[] {MiniPLTokenType.End })]
        [InlineData("read", new MiniPLTokenType[] {MiniPLTokenType.Read })]
        [InlineData("print", new MiniPLTokenType[] {MiniPLTokenType.Print })]
        [InlineData("assert", new MiniPLTokenType[] {MiniPLTokenType.Assert })]
        [InlineData("int", new MiniPLTokenType[] {MiniPLTokenType.IntType })]
        [InlineData("string", new MiniPLTokenType[] {MiniPLTokenType.StringType })]
        [InlineData("bool", new MiniPLTokenType[] {MiniPLTokenType.BoolType })]
        [InlineData("", new MiniPLTokenType[] {MiniPLTokenType.EOF, MiniPLTokenType.EOF})]
        [InlineData("var I : int := 1234567890", new MiniPLTokenType[] {
            MiniPLTokenType.Var, MiniPLTokenType.Identifier,
            MiniPLTokenType.Colon, MiniPLTokenType.IntType,
            MiniPLTokenType.AssignmentOperator, MiniPLTokenType.IntegerValue,
            MiniPLTokenType.EOF
        })]
        [InlineData("for i in 1..n do end for", new MiniPLTokenType[] {
            MiniPLTokenType.For, MiniPLTokenType.Identifier,
            MiniPLTokenType.In, MiniPLTokenType.IntegerValue, MiniPLTokenType.DotDot, MiniPLTokenType.Identifier,
            MiniPLTokenType.Do, MiniPLTokenType.End, MiniPLTokenType.For,
            MiniPLTokenType.EOF
        })]
        [InlineData("123a321", new MiniPLTokenType[] { MiniPLTokenType.IntegerValue, MiniPLTokenType.Identifier })]
        public void TestNextTokens(string matchee, MiniPLTokenType[] expectedResults)
        {
            new TestUtil<MiniPLTokenType>().TestTokens(new MiniPLLexer(), matchee, expectedResults);
        }

        [Theory]
        [InlineData("var var",
            new int[] { 1, 1 },
            new int[] { 1, 5 }
        )]
        [InlineData("var\nvar",
            new int[] { 1, 2 },
            new int[] { 1, 1 }
        )]
        [InlineData("var \r\n var",
            new int[] { 1, 2 },
            new int[] { 1, 2 }
        )]
        [InlineData("var I : \nint := 1234567890\n\tend for",
            new int[] {1, 1, 1, 2, 2, 2, 3, 3},
            new int[] {1, 5, 7, 1, 5, 8, 2, 6}
        )]
        public void TestPositions(string matchee, int[] expectedRows, int[] expectedColumns)
        {
            new TestUtil<MiniPLTokenType>().TestPositions(new MiniPLLexer(), matchee, expectedRows, expectedColumns);
        }

        /*
        public enum MiniPLTokenType
        {
            Identifier,
            Colon,
            DotDot,
            AssignmentOperator,
            PlusOperator,
            MinusOperator,
            TimesOperator,
            DivisionOperator,
            LessThanOperator,
            EqualsOperator,
            AndOperator,
            NotOperator,
            Semicolon,
            ParenthesisOpen,
            ParenthesisClose,
            IntegerValue,
            StringValue,
            Whitespace,
            CommentedLine,
            MultilineComment,
            Var,
            For,
            In,
            Do,
            End,
            Read,
            Print,
            Assert,
            IntType,
            StringType,
            BoolType,
            EOF
        }
        */
    }
}