using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MiniPL;
using MiniPL.Parsing;
using MiniPL.Traversing;
using Xunit;

namespace MiniPLTester.Traversing
{
    public class TreeNodeImpl : TreeNode<TreeNodeImpl> {
        public readonly int id;

        public TreeNodeImpl(int id) {
            this.id = id;
        }
    }

    public class TestUtil {
        public TreeNodeImpl CreateTree(int[] childNodes)
        {
            int id = 0;
            IList<TreeNodeImpl> nodeList = new List<TreeNodeImpl> {new TreeNodeImpl(id++)};
            for (int i = 0; i < childNodes.Length; i++)
            {
                TreeNodeImpl parent = nodeList[i];
                for (int n = 0; n < childNodes[i]; n++) {
                    TreeNodeImpl child = new TreeNodeImpl(id++);
                    if (parent.Down() == null)
                        parent.Down(child);
                    else
                        parent.DownRight().Right(child);
                    child.Up(parent);
                    nodeList.Add(child);
                }
            }
            return nodeList[0];
        }

        public void AssertSequence(ITreeTraverser<TreeNodeImpl> traverser, int[] ids)
        {
            TreeNodeImpl node;
            int index = 0;
            while ((node = traverser.Next()) != null) {
                Assert.Equal(ids[index], node.id);
                index++;
            }
            Assert.Equal(index, ids.Length);
        }
    }

    public class TestDepthFirstPostOrderTreeTraverser
    {
        [Fact]
        public void Test1()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { });
            var traverser = new DepthFirstPostOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 0 });
        }

        [Fact]
        public void Test2()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 1 });
            var traverser = new DepthFirstPostOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 1, 0 });
        }

        [Fact]
        public void Test3()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 3 });
            var traverser = new DepthFirstPostOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 1, 2, 3, 0 });
        }

        [Fact]
        public void Test4()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 1, 1, 1 });
            var traverser = new DepthFirstPostOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 3, 2, 1, 0 });
        }

        [Fact]
        public void Test5()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 2, 2, 1, 0, 1 });
            var traverser = new DepthFirstPostOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 3, 6, 4, 1, 5, 2, 0 });
        }

        [Fact]
        public void Test6()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1 });
            var traverser = new DepthFirstPostOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 4, 5, 6, 1, 7, 8, 9, 2, 10, 11, 13, 12, 3, 0 });
        }
    }

    public class TestDepthFirstPreOrderTreeTraverser
    {
        [Fact]
        public void Test1()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { });
            var traverser = new DepthFirstPreOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 0 });
        }

        [Fact]
        public void Test2()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 1 });
            var traverser = new DepthFirstPreOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 0, 1 });
        }

        [Fact]
        public void Test3()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 3 });
            var traverser = new DepthFirstPreOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 0, 1, 2, 3 });
        }

        [Fact]
        public void Test4()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 1, 1, 1 });
            var traverser = new DepthFirstPreOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 0, 1, 2, 3 });
        }

        [Fact]
        public void Test5()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 2, 2, 1, 0, 1 });
            var traverser = new DepthFirstPreOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 0, 1, 3, 4, 6, 2, 5 });
        }

        [Fact]
        public void Test6()
        {
            var util = new TestUtil();
            TreeNodeImpl root = util.CreateTree(new int[] { 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1 });
            var traverser = new DepthFirstPreOrderTreeTraverser<TreeNodeImpl>(root);
            util.AssertSequence(traverser, new int[] { 0, 1, 4, 5, 6, 2, 7, 8, 9, 3, 10, 11, 12, 13 });
        }
    }
}